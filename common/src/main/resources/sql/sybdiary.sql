/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : 127.0.0.1:3306
 Source Schema         : sybdiary

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 07/06/2022 21:01:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for approve
-- ----------------------------
DROP TABLE IF EXISTS `approve`;
CREATE TABLE `approve`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `approve_part` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请部门',
  `approve_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请人',
  `approve_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请单描述',
  `gmt_create` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of approve
-- ----------------------------

-- ----------------------------
-- Table structure for myfile
-- ----------------------------
DROP TABLE IF EXISTS `myfile`;
CREATE TABLE `myfile`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'key',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件原名称',
  `file_new_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件新名称',
  `file_all_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件全路径',
  `file_size` double NULL DEFAULT NULL COMMENT '文件大小',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of myfile
-- ----------------------------
INSERT INTO `myfile` VALUES (13, 'Screenshot_20220512_081125_com.ss.android.ugc.aweme.jpg', '3627285b-9d1b-4e71-9614-fbcdfa55931b-2022-05-12.jpg', 'c:\\upload\\3627285b-9d1b-4e71-9614-fbcdfa55931b-2022-05-12.jpg', 1201584, NULL);

-- ----------------------------
-- Table structure for order_import
-- ----------------------------
DROP TABLE IF EXISTS `order_import`;
CREATE TABLE `order_import`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 350002 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_import
-- ----------------------------

-- ----------------------------
-- Table structure for svi_article
-- ----------------------------
DROP TABLE IF EXISTS `svi_article`;
CREATE TABLE `svi_article`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `category_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别 ID',
  `label_list` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签 / 关键字',
  `status` int(255) NULL DEFAULT NULL COMMENT '1-新发布；2-进行中；3-已完成；4-取消；',
  `created_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 243105 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of svi_article
-- ----------------------------
INSERT INTO `svi_article` VALUES (243096, 'BMC-git', '<p>http://bitbucket.remedyapi.cn:11990/login?next=/projects/BOEIT/repos/financeinterface/browse</p>\n<p>账号：Andrew</p>\n<p>密码：Uf023895&nbsp;&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243097, '财务数据更新步骤', '<p>1、修改 sql 视图</p>\n<p>2、导入viewForm新增字段</p>\n<p>3、在Regular Form 中新增相应的字段</p>\n<p>4、修改jar 包的db.properties</p>\n<p>5、测试数据&nbsp;</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243098, 'BOC', '<p><span lang=\"EN-US\" style=\"font-size: 10.5pt; mso-bidi-font-size: 12.0pt; font-family: \'Calibri\',sans-serif; mso-fareast-font-family: 宋体; mso-bidi-font-family: \'Times New Roman\'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA;\"><a href=\"https://rdpb.bocsoft.cn:7443/PasswordVault/\"><span style=\"font-size: 14.0pt; font-family: 宋体; mso-bidi-font-family: 宋体;\">https://rdpb.bocsoft.cn:7443/PasswordVault/</span></a></span></p>\n<p><span style=\"font-size: 14pt;\"><strong><em><span lang=\"EN-US\" style=\"font-family: Calibri, sans-serif;\">s</span></em><span lang=\"EN-US\" style=\"font-family: Calibri, sans-serif;\">t-wb-wy0128</span><span lang=\"EN-US\" style=\"font-family: Calibri, sans-serif;\"><br>Itsmboc04!<br>22.11.146.17</span></strong></span></p>\n<p><span style=\"font-size: 14pt;\"><strong><span lang=\"EN-US\" style=\"font-family: Calibri, sans-serif;\">测试cache密码：arsystem</span></strong></span></p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243099, 'BOC Inode 接口', '<h3>1、新增</h3>\n<p>根据标识字段，严格约束调用接口的标识（枚举），接口回写调用状态，Inode 账号判断是否重复，不可重复添加</p>\n<h3>2、注销</h3>\n<p>确认表的触发条件，根据对应的字段逻辑，调用接口的删除</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243100, 'RSSO', '<p>账号：Admin</p>\n<p>密码：B@Eremedy2002</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243101, '可视化配置', '<p>1、在 ITAM 系统首页菜单栏，增加可视化看板链接</p>\n<p>2、修改嵌入可视化页面嵌入后的配置表单</p>\n<p>3、修改财务数据接口，增加字段</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243102, 'test', '<p>aaaa</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243103, '共享盘账号密码', '<p><strong>账号：</strong>administrator</p>\n<p><strong>密码：</strong>Itsmboc01！</p>', NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `svi_article` VALUES (243104, 'INdeo接口', '<p>1、Add : 核对传递字段</p>\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;校验的查询方式，查询本地表还是对端系统</p>\n<p>2、DEL ： 有时候会卡掉，确实是否需要异步处理，（工单关闭，SRM：Request (服务请求单没有关闭)）</p>', NULL, NULL, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for svi_category
-- ----------------------------
DROP TABLE IF EXISTS `svi_category`;
CREATE TABLE `svi_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别id',
  `category_par_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别父级id',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of svi_category
-- ----------------------------

-- ----------------------------
-- Table structure for svi_label
-- ----------------------------
DROP TABLE IF EXISTS `svi_label`;
CREATE TABLE `svi_label`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键 id',
  `label_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签 id',
  `label_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of svi_label
-- ----------------------------
INSERT INTO `svi_label` VALUES (1, 'c20097de-e375-4052-95c8-b19f17c5d5d3', 'URL');
INSERT INTO `svi_label` VALUES (2, '690c219b-4d0d-442f-9712-dcaf8a7455cb', '工作');
INSERT INTO `svi_label` VALUES (3, 'd4739ab1-6508-4e06-be0b-367881fc1c22', '记录');
INSERT INTO `svi_label` VALUES (4, '0439146d-4e2e-453d-9522-bd8cb6ae0354', '日常');
INSERT INTO `svi_label` VALUES (5, 'e2bd0ab4-886e-4bea-a9c7-aa217de63279', '账号密码');
INSERT INTO `svi_label` VALUES (6, '4c72679c-61e2-4b59-8844-09dd337581ff', '地址');
INSERT INTO `svi_label` VALUES (7, '48fac129-0d0c-4dfe-8f2c-b4399e69d544', '操作步骤');
INSERT INTO `svi_label` VALUES (8, '82eb6f9d-e639-4979-be58-f2abf7394180', '代办');
INSERT INTO `svi_label` VALUES (9, '625b1531-a188-4c8e-9050-7625d5468751', '备忘录');

-- ----------------------------
-- Table structure for svi_user
-- ----------------------------
DROP TABLE IF EXISTS `svi_user`;
CREATE TABLE `svi_user`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一主键',
  `user_name` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `openId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信 or QQ Id',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `sign` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户签名',
  `sex` int(1) NULL DEFAULT NULL COMMENT '性别（1）男（0）女',
  `identity` int(1) NULL DEFAULT NULL COMMENT '用户身份  1老师 2 学生',
  `is_disable` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'false' COMMENT '是否注销（true）注销（false）启用',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上次修改者',
  `gmt_create` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of svi_user
-- ----------------------------
INSERT INTO `svi_user` VALUES ('1', 'admin', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, 'false', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
