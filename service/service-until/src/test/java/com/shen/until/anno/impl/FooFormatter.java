package com.shen.until.anno.impl;


import com.shen.until.anno.Format;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component("fooFormatter")
@Primary
public class FooFormatter implements Format {

    @Override
    public String format() {
        return "fooFormatter";
    }
}
