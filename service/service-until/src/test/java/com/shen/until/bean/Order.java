package com.shen.until.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.shen.until.converter.MyConverter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Order {

    private Integer id;
    @ExcelProperty(converter = MyConverter.class)
    private String orderId;
    private String userName;
    /**
     * 这里用string 去接日期才能格式化。我想接收年月日格式
     * 格式化之后的 pattern
     */
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private String createdTime;
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private String updateTime;
}
