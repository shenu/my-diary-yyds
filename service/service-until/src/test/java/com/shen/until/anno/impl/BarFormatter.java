package com.shen.until.anno.impl;

import com.shen.until.anno.Format;
import org.springframework.stereotype.Component;

@Component("barFormatter")
public class BarFormatter implements Format {
    @Override
    public String format() {
        return "barFormatter";
    }
}
