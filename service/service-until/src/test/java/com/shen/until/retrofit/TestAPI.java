package com.shen.until.retrofit;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Auther
 * @createdate 2023/8/14
 * @description
 */
public class TestAPI {

    @Autowired HttpApi httpApi;

    @Test
    public void test1(){
        String data = httpApi.getUserAll(1, 5);
        System.out.println(data);
    }
}
