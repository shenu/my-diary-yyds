package com.shen.until.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Index2Data {

    @ExcelProperty(value = {"父表头","key_id"},index = 0)
    private String orderId;
    @ExcelProperty(value = {"父表头","name"},index = 1)
    private String userName;
    @ExcelProperty(value = {"父表头","createdTime"},index = 3)
    private String createdTime;
    @ExcelProperty(value = {"父表头","updateTime"},index = 4)
    private String updateTime;
}
