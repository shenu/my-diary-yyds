package com.shen.until.anno;

import com.shen.until.anno.Format;
import com.shen.until.anno.impl.BarFormatter;
import com.shen.until.anno.impl.FooFormatter;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 测试注解
 * @author shen
 */
@SpringBootTest
public class TestAnno {

    @Autowired
    private ShenConfiguration config;

    @Qualifier("barFormatter")
    @Autowired
    private Format format;

    @Test
    public void test(){

        System.out.println(format.format());
    }

    @Test
    public void getShenConfig(){

        System.out.println(config.getShen());
    }


    @Test
    @SneakyThrows()
    // 12小时制 转 24 小时制 12:00:00 AM 和 12:00:00 PM 都需要处理
    public void testTransactionDate(){
        String a = "11/6/2022 12:00:00 PM";
//        String a = "11/6/2022 11:00:00 AM";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a", Locale.ENGLISH);
//        System.out.println(sdf.format(new Date()));
        int i = a.indexOf("12:00:00 AM");
        if (a.indexOf("12:00:00 AM") > 0){
            String[] split = a.split(" ");
            if (split != null && split.length > 0){
                SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
                Date parse = format1.parse(split[0]);
                SimpleDateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
                String format = f2.format(parse);
                a = format+" 12:00:00";
                System.out.println(a);
            }
        }else{
            Date date = sdf.parse(a);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println(format.format(date));
        }



    }

}
