package com.shen.until.security;

import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Random;
import java.util.UUID;

@SpringBootTest
public class AES {
    private static final String ALGORITHM = "AES";

    /**
     * AES加密字符串
     * @param  password 密钥
     * @param value 待加密字符串
     */
    public static String encrypt(String password, String value) throws Exception {
        Key key = generateKey(password);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedByteValue = cipher.doFinal(value.getBytes("utf-8"));
        String encryptedValue64 = Base64.getEncoder().encodeToString(encryptedByteValue);
        return encryptedValue64;
    }

    /**
     * AES解密字符串
     * @param  password 密钥
     * @param value 待解密字符串
     * @return
     */
    public static String decrypt(String password, String value) throws Exception {
        Key key = generateKey(password);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedValue64 = Base64.getDecoder().decode(value);
        byte[] decryptedByteValue = cipher.doFinal(decryptedValue64);
        String decryptedValue = new String(decryptedByteValue,"utf-8");
        return decryptedValue;
    }

    /**
     * AES加密文件
     * @param password 密钥
     * @param inputFile 待加密文件路径
     * @param outputFile 输出文件路径
     */
    public static void encrypt(String password,String inputFile, String outputFile) throws Exception {
        Key key = generateKey(password);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        FileInputStream inputStream = new FileInputStream(inputFile);
        byte[] inputBytes = new byte[(int) inputFile.length()];
        inputStream.read(inputBytes);

        byte[] outputBytes = cipher.doFinal(inputBytes);

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        outputStream.write(outputBytes);

        inputStream.close();
        outputStream.close();
    }

    /**
     * AES解密文件
     * @param password 密钥
     * @param inputFile 待解密文件路径
     * @param outputFile 输出文件路径
     */
    public static void decrypt(String password, String inputFile, String outputFile) throws Exception{
        Key key = generateKey(password);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);

        FileInputStream inputStream = new FileInputStream(inputFile);
        byte[] inputBytes = new byte[(int) inputFile.length()];
        inputStream.read(inputBytes);

        byte[] outputBytes = cipher.doFinal(inputBytes);

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        outputStream.write(outputBytes);

        inputStream.close();
        outputStream.close();
    }

    /**
     * 生成key
     * @param password
     * @return
     * @throws Exception
     */
    private static Key generateKey(String password) throws Exception {
        Key key = new SecretKeySpec(password.getBytes(),ALGORITHM);
        return key;
    }


    public static void main(String[] args) throws Exception {
        String originalString = "hello world";
        String key = getRandomStr(32);
        System.out.println(key);
        String encryptedString = AES.encrypt(key, originalString);
        String decryptedString = AES.decrypt(key, encryptedString);

        System.out.println(originalString);
        System.out.println(encryptedString);
        System.out.println(decryptedString);

    }

    public static String getRandomStr(int n){
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random r = new Random();//创建random对象
        StringBuffer buff = new StringBuffer();//StringBuffer
        for (int i = 0; i < n; i++) {
            int it = r.nextInt(62);//使用random生成[0,62)之间的随机数,不包括62
            buff.append(str.charAt(it));// 把int下标 转为 str中随机字符(数字，大写字母或者小写字母)

        }
        return buff.toString();
    }

}