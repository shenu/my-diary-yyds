package com.shen.until.easyexcellistener.write;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.alibaba.excel.write.style.column.SimpleColumnWidthStyleStrategy;
import com.shen.until.bean.Article;
import com.shen.until.bean.ArticleExcel;
import com.shen.until.bean.Index2Data;
import com.shen.until.bean.IndexData;
import com.shen.until.bean.xslx.OrderExcel;
import com.shen.until.service.ArticleService;
import com.shen.until.service.ExcelService;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
public class TestWriteExcel {

    @Autowired
    private ExcelService excelService;

    @Autowired
    private ArticleService articleService;

    String excelName_prefix = "c:\\upload\\test-write-";
    String excelName_suffix = ".xlsx";
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public List<OrderExcel> getData(){

        List<OrderExcel> list = excelService.list(null);
        System.out.println("list");
        return list;
    }

    @Test
    public void test(){
        getData();
    }

    @Test
    public void test1(){

        /**
         * 注意 simpleWrite 在数据量不大的情况下可以使用（5000以内，具体看实际情况，数据量大，参照多次重复写入）
         *
         */

        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+excelName_suffix;
        //这里需要指定用那个class 去写，然后写到第一个sheet，名字为模板，然后文件流会自动关闭
        //如果这里想使用03，则传入 excelType 参数即可
        File file = new File(excelName);

        EasyExcel.write(excelName, OrderExcel.class )
                .sheet("order")
                .doWrite(()->{
                    return getData();
                });
    }

    //写法2
    @Test
    public void test2(){
        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+"--"+System.currentTimeMillis()+excelName_suffix;
        EasyExcel.write(excelName,OrderExcel.class).sheet("模板").doWrite(getData());
    }

    //写法3
    @Test
    public void test3(){
        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+"--"+System.currentTimeMillis()+excelName_suffix;
        ExcelWriter writer = null;
        try {
            writer = EasyExcel.write(excelName,OrderExcel.class).build();
            WriteSheet writeSheet = EasyExcel.writerSheet("模板").build();
            writer.write(getData(),writeSheet);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //必须关闭流
            if (writer != null){
                writer.finish();
            }
        }

    }

    /**
     * 根据参数到处指定的列
     * 根据自己或者排除自己需要的列
     * 直接写即可
     */
    @Test
    public void test4(){
        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+"--"+System.currentTimeMillis()+excelName_suffix;
        //根据用户传入的字段，假设我们需要忽略时间字段
        Set<String> excludeColumn = new HashSet<>();
        //需要注意的是 这里传的是实体的属性名称
        excludeColumn.add("createdTime");
        excludeColumn.add("updateTime");
        EasyExcel.write(excelName,OrderExcel.class).excludeColumnFiledNames(excludeColumn)
                .sheet("sheet1").doWrite(this::getData);

    }
    /**
     * 指定需要写入的列
     */
    @Test
    public void test5(){
        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+"--"+System.currentTimeMillis()+excelName_suffix;
        EasyExcel.write(excelName, IndexData.class).sheet("自定义写出").doWrite(this::getData);
    }

    /**
     * 创建excel 对应的实体表头对象
     * 复杂标题头写入
     *
     */
    @Test
    public void test6(){
        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+"--"+System.currentTimeMillis()+excelName_suffix;
        EasyExcel.write(excelName, Index2Data.class).sheet("自定义写出").doWrite(this::getData);
    }

    /**
     * 重复多次的写入 同一个sheet 页
     */





    /**
     * test 导出文章数据
     */
    @Test
    public void test7(){
        List<Article> list = articleService.list(null);
        List<ArticleExcel> articleExcels = new ArrayList<>();
        list.forEach(article -> {
            ArticleExcel excel = new ArticleExcel();
            BeanUtils.copyProperties(article,excel);
            articleExcels.add(excel);
        });
        String format = dateFormat.format(new Date());
        String excelName = excelName_prefix+format+"--"+System.currentTimeMillis()+excelName_suffix;
        //这里需要指定用那个class 去写，然后写到第一个sheet，名字为模板，然后文件流会自动关闭
        EasyExcel.write(excelName, ArticleExcel.class).sheet("自定义写出").doWrite(articleExcels);

    }
    /**
     * 不创建对象的写
     */
    @Test
    public void test8(){

        // 写法1
        String fileName ="noModelWrite" + System.currentTimeMillis() + ".xlsx";
        //设置表头样式
        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        WriteFont writeFont = new WriteFont();
        writeFont.setBold(false);
        headWriteCellStyle.setWriteFont(writeFont);
        // 背景设置为红色
        headWriteCellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        headWriteCellStyle.setBorderBottom(BorderStyle.DASH_DOT);
        headWriteCellStyle.setBorderLeft(BorderStyle.DOTTED);
        headWriteCellStyle.setBorderRight(BorderStyle.THICK);
        headWriteCellStyle.setBorderTop(BorderStyle.DOUBLE);
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        // 这里需要指定 FillPatternType 为FillPatternType.SOLID_FOREGROUND 不然无法显示背景颜色.头默认了 FillPatternType所以可以不指定
//        contentWriteCellStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
//        contentWriteCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        HorizontalCellStyleStrategy horizontalCellStyleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle,contentWriteCellStyle);
        EasyExcel.write(fileName).useDefaultStyle(false)
                .registerWriteHandler(new SimpleColumnWidthStyleStrategy(20))
                .head(head()).sheet("模板").doWrite(dataList());



    }
    private List<List<String>> head() {
        List<List<String>> list = ListUtils.newArrayList();
        List<String> head0 = ListUtils.newArrayList();
        head0.add("字符串" + System.currentTimeMillis());
        List<String> head1 = ListUtils.newArrayList();
        head1.add("数字" + System.currentTimeMillis());
        List<String> head2 = ListUtils.newArrayList();
        head2.add("日期" + System.currentTimeMillis());
        list.add(head0);
        list.add(head1);
        list.add(head2);
        return list;
    }
    private List<List<Object>> dataList() {
        List<List<Object>> list = ListUtils.newArrayList();
        for (int i = 0; i < 10; i++) {
            List<Object> data = ListUtils.newArrayList();
            data.add("字符串" + i);
            data.add(new Date().toString());
            data.add(0.56);
            list.add(data);
        }
        return list;
    }
}
