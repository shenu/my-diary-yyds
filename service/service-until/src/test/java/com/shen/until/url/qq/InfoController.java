package com.shen.until.url.qq;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther
 * @createdate 2022/12/27
 * @description
 */
public class InfoController {

    public static void main(String[] args) {
//        try {
//            URL qq = new URL("https://v.qq.com/biu/u/history/");
//            URLConnection connection = qq.openConnection();
//            InputStream inputStream = connection.getInputStream();
//            String encoding = connection.getContentEncoding();
//            BufferedReader br=new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
//            //按行读取并打印
//            String line=null;
//            while((line=br.readLine())!=null){
//                System.out.println(line);
//            }
//            br.close();
//
//
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }

        try {
            Document document = Jsoup.connect("https://v.qq.com/biu/u/history/").header("Cookie", "video_guid=536842f5aa1fdbfe; tvfe_boss_uuid=8b2b44fe494e27ac; pgv_pvid=1626556471; ts_uid=9683733048; video_platform=2; RK=2GuRr3BROw; ptcz=501333ead5eca667c43969807c1cd2a28e8526caa8bee2cf8a386706c2fd5b8c; main_login=qq; vqq_vuserid=422801171; vqq_access_token=8A41C02E080D10865680AE2571C12419; vqq_openid=C8472214BA70A04ECCE2385AB9D54734; vqq_appid=101483052; qq_nick=須 ぼ 臾; qq_head=https://tvpic.gtimg.cn/head/453db7ac8cfbe3eb210d4a80620d8b222dcea0a8eab38862180c3141c58b2cee56a27a9a/422; tvfe_search_uid=c5ecef2b-1d89-4ed6-9cae-3c70117771ef; vip_expinfo_str=expid=9217522&status=2; bucket_id=9231001; ts_refer=www.baidu.com/link; tab_experiment_str=9217522#9428073; fqm_pvqid=df663881-09f9-4e20-a671-346c73db80a5; pgv_info=ssid=s3310631860; vqq_vusession=bj05c58Is4fN3mJq5hgN_g.N; ts_last=v.qq.com/biu/u/history/; uid=353040035; ptag=|biu").get();
            System.out.println(document.data());
            String s1 = "video_guid=536842f5aa1fdbfe; tvfe_boss_uuid=8b2b44fe494e27ac; pgv_pvid=1626556471; ts_uid=9683733048; video_platform=2; RK=2GuRr3BROw; ptcz=501333ead5eca667c43969807c1cd2a28e8526caa8bee2cf8a386706c2fd5b8c; main_login=qq; vqq_vuserid=422801171; vqq_access_token=8A41C02E080D10865680AE2571C12419; vqq_openid=C8472214BA70A04ECCE2385AB9D54734; vqq_appid=101483052; qq_nick=須 ぼ 臾; qq_head=https://tvpic.gtimg.cn/head/453db7ac8cfbe3eb210d4a80620d8b222dcea0a8eab38862180c3141c58b2cee56a27a9a/422; tvfe_search_uid=c5ecef2b-1d89-4ed6-9cae-3c70117771ef; vip_expinfo_str=expid=9217522&status=2; bucket_id=9231001; ts_refer=www.baidu.com/link; tab_experiment_str=9217522#9428073; fqm_pvqid=df663881-09f9-4e20-a671-346c73db80a5; pgv_info=ssid=s3310631860; vqq_vusession=bj05c58Is4fN3mJq5hgN_g.N; ts_last=v.qq.com/biu/u/history/; uid=353040035; ptag=|biu";
            Map<String,String> map = new HashMap<>();
            String[] split = s1.split(";");
            for (String s : split) {
                String[] split1 = s.split("=");
                map.put(split1[0],split1[1]);
            }


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
