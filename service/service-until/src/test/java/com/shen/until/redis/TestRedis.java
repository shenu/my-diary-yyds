package com.shen.until.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class TestRedis {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test1(){

        redisTemplate.opsForValue().set("test","Shen");
        System.out.println(redisTemplate.opsForValue().get("test"));
    }

    @Test
    public void test2(){

        String str = "c:\\upload\\a157f446-3670-4428-bb62-e2abccdb1add-2022-05-05.html";
        System.out.println(str.substring(str.lastIndexOf("\\")+1,str.length()));
    }
}
