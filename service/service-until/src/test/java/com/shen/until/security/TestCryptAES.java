package com.shen.until.security;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @Auther
 * @createdate 2023/5/19
 * @description
 */
@SpringBootTest
public class TestCryptAES {

    private static final String AES_ALGORITHM = "AES";

    private static final String UTF8 = StandardCharsets.UTF_8.name();

    private String secret = "secret";

    @Test
    public void encryptAES(){

        String content = "test 2023-05-19";
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        //加密
        byte[] encrypt = aes.encrypt(content);

        //解密
        byte[] decrypt = aes.decrypt(encrypt);

        String encryptHex = aes.encryptHex(content);
        String decryptStr = aes.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);
        System.out.println(encryptHex);
        System.out.println(decryptStr);

    }

    /* AES加密 String */
    @SneakyThrows
    public static String encryptStrAES(String text, String key){
        if(text == null || "".equals(text)){
            throw new Exception("encode text should not be null or empty.");
        }
        byte[] encodeBytes = encryptByteAES(text.getBytes(StandardCharsets.UTF_8), key);
        return Base64.encodeBase64String(encodeBytes);
    }

    /* AES解密 String*/
    @SneakyThrows
    public static String decryptStrAES(String text, String key){
        if(text == null || "".equals(text)){
            throw new Exception("encode text should not be null or empty.");
        }
        byte[] decodeBytes = decryptByteAES(Base64.decodeBase64(text.getBytes(StandardCharsets.UTF_8)), key);
        return new String(decodeBytes, StandardCharsets.UTF_8);
    }
    /* AES加密 originalBytes */
    @SneakyThrows
    public static byte[] encryptByteAES(byte[] originalBytes, String key){
        if(ArrayUtils.isEmpty(originalBytes)){
            throw new Exception("encode text should not be null or empty.");
        }
        if(key == null || "".equals(key)){
            throw new Exception("encode text should not be null or empty.");
        }
        Cipher cipher = getAESCipher(key, Cipher.ENCRYPT_MODE);
        byte[] encodeBytes = null;
        try {
            encodeBytes = cipher.doFinal(originalBytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            new Exception(e.getClass().getName()+": encode byte fail. "+e.getMessage());
        }
        return encodeBytes;
    }

    /* AES解密 encryptedBytes */
    public static byte[] decryptByteAES(byte[] encryptedBytes, String key){
        if(ArrayUtils.isEmpty(encryptedBytes)){
            new Exception("decode encryptedBytes should not be empty.");
        }
        if(key == null || "".equals(key)){
            new Exception("key :" + key + ", decode key should not be null or empty.");
        }
        Cipher cipher = getAESCipher(key, Cipher.DECRYPT_MODE);
        byte[] decodeBytes = null;
        try {
            decodeBytes = cipher.doFinal(encryptedBytes);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            new Exception(e.getClass().getName()+": decode byte fail. "+e.getMessage());
        }
        return decodeBytes;
    }
    public static Cipher getAESCipher(String key, int mode){
        if(key == null || "".equals(key)){
            new Exception("key :" + key + ", should not be null or empty.");
        }
        Cipher cipher = null;
        SecretKey secretKey;
        try {
            cipher = Cipher.getInstance(AES_ALGORITHM);
            byte[] keyBytes = key.getBytes(UTF8);
            secretKey = new SecretKeySpec(keyBytes, AES_ALGORITHM);
            cipher.init(mode, secretKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            new Exception(e.getClass().getName()+": get cipher instance wrong. "+e.getMessage());
        } catch (UnsupportedEncodingException u){
            new Exception(u.getClass().getName()+": key transfer bytes fail. "+u.getMessage());
        } catch (InvalidKeyException i) {
            new Exception(i.getClass().getName()+": key is invalid. "+i.getMessage());
        }
        return cipher;
    }

}
