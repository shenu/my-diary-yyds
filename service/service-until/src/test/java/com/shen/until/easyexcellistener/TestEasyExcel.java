package com.shen.until.easyexcellistener;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import com.shen.until.bean.Order;
import com.shen.until.bean.xslx.OrderExcel;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@Slf4j
public class TestEasyExcel {

    private static File file = new File("C:\\Users\\10353\\Desktop\\order_import.xlsx");
    @Test
    public void write1(){


        EasyExcel.read(file, OrderExcel.class,new PageReadListener<OrderExcel>(list ->{
            for (OrderExcel item : list) {
                log.info("读取到一条数据{}", JSON.toJSONString(item));
            }
        })).sheet().doRead();
    }

    @Test
    public void write2(){

        EasyExcel.read(file, OrderExcel.class,new ReadListener<OrderExcel>(){
            @Override
            public void invoke(OrderExcel orderExcel, AnalysisContext analysisContext) {

                log.info("读取到一条数据{}", JSON.toJSONString(orderExcel));
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.info("解析完所有数据之后的回调" + JSON.toJSONString(analysisContext));
            }

        }).sheet().doRead();
    }

    @Test
    public void write3(){

        EasyExcel.read(file, OrderExcel.class,new OrderListener()).sheet().doRead();
    }

    @Test
    public void write4(){
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(file,OrderExcel.class,new OrderListener()).build();
            ReadSheet readSheet = EasyExcel.readSheet(0).build();
            excelReader.read(readSheet);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //这里会产生临时文件，必须关闭
            if (excelReader != null){
                excelReader.finish();
            }

        }
    }

    /**
     * 读取多个或者全部 sheet，这里需要注意一个sheet 不能够读取多次，多次读取需要重新读取文件
     *
     */
    @Test
    public void readMoreSheets1(){

        // 这里需要注意的是在读取完一个sheet 页之后，doAfterAllAnalysed 会在每个sheet读取完毕后调用一次。
        EasyExcel.read(file,OrderExcel.class,new OrderListener()).doReadAll();
    }

    @Test
    public void readMoreSheets2(){

        // 这里需要注意的是在读取完一个sheet 页之后，doAfterAllAnalysed 会在每个sheet读取完毕后调用一次。
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(file).build();
            ReadSheet sheet1 = EasyExcel.readSheet(0).head(OrderExcel.class).registerReadListener(new OrderListener()).build();
            ReadSheet sheet2 = EasyExcel.readSheet(1).head(OrderExcel.class).registerReadListener(new OrderListener()).build();
            excelReader.read(sheet1,sheet2);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //记得关闭临时文件
            // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
            if (excelReader != null){
                excelReader.finish();
            }
        }

    }

    @Test
    public void readConvention1(){
        EasyExcel.read(file, Order.class,new PageReadListener<Order>( lists ->{
            lists.forEach(item -> {
                log.info("格式化的记录：" + item);
            });
        })).sheet(0).doRead();
    }


    @Test
    public void syncReadData(){
        List<Order> list = EasyExcel.read(file).head(Order.class).sheet().doReadSync();
//        System.out.println(list);

        List<Map<String, String>> mapList = EasyExcel.read(file).sheet().doReadSync();
        mapList.forEach(item -> {
            System.out.println(item);
        });
    }
    

    @Test
    public void noModelRead(){

        EasyExcel.read(file,new NoModelListener()).sheet().doRead();
    }

}
