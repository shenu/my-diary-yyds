package com.shen.until;

import com.alibaba.excel.EasyExcel;
import com.shen.until.bean.DemoData;
import com.shen.until.bean.IndexOrNameData;
import com.shen.until.easyexcellistener.DemoDataListener;
import com.shen.until.easyexcellistener.OrderListener1;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

@SpringBootTest
public class FileTest {

    /**
     * 测试普通处理excel
     */
    @Test
    public void excelDeal1(){

        String filePath = "C:\\Users\\Administrator\\Desktop\\test.xlsx";
        File file = new File(filePath);

        EasyExcel.read(file,DemoData.class,new DemoDataListener()).sheet().doRead();
    }

    /**
     * 指定列的下标或者列名 处理excel
     */
    @Test
    public void excelDeal2(){

        String filePath = "C:\\Users\\Administrator\\Desktop\\order.xlsx";
        EasyExcel.read(filePath, IndexOrNameData.class,new OrderListener1()).sheet().doRead();
    }
}
