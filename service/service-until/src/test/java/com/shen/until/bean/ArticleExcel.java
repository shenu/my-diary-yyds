package com.shen.until.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
public class ArticleExcel {

    private Integer id;
    private String title;
    private String contentMsg;
    private String categoryId;
    private String LabelList;
    private Integer status;
    private String createdUser;

}
