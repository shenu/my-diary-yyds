package com.shen.until.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class IndexData {

    @ExcelProperty(value = "key_id",index = 0)
    private String orderId;
    @ExcelProperty(value = "name",index = 1)
    private String userName;
    @ExcelProperty(value = "createdTime",index = 3)
    private String createdTime;
    @ExcelProperty(value = "updateTime",index = 4)
    private String updateTime;

}
