package com.shen.until.anno;

public interface Format {
    public String format();
}
