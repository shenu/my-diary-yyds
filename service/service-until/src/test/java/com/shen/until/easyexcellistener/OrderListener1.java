package com.shen.until.easyexcellistener;


import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.shen.until.bean.IndexOrNameData;
import lombok.extern.slf4j.Slf4j;

/**
 * 指定列的下标或者列名
 */
@Slf4j
public class OrderListener1 implements ReadListener<IndexOrNameData> {


    @Override
    public void invoke(IndexOrNameData indexOrNameData, AnalysisContext analysisContext) {

        log.info("读取到记录如下："+indexOrNameData);

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

        log.info("保存记录");
    }


}

