package com.shen.until.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class IndexOrNameData{

    /**
     *  强制读取第三个 这里不建议 index 和 name 同时用，要么一个对象只用index，
     *  要么一个对象只用name去匹配
     *  用名字去匹配，这里需要注意，如果名字重复，会导致只有一个字段读取到数据
     */
    @ExcelProperty(value = "id")
    private Integer id;
    @ExcelProperty(value = "order_id")
    private String orderId;
    @ExcelProperty(value = "user_name")
    private String userName;
    @ExcelProperty(value = "created_time")
    private String createdTime;
    @ExcelProperty(value = "update_time")
    private String updateTime;
}