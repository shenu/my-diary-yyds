package com.shen.until;

import lombok.SneakyThrows;
import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class UntilApplicationTests {

    /**
     * XML文档在DOM解析中可以被映射为多种节点，其中比较重要和常见的是元素节点(Element）、
     * 属性节点（Attribute）和文本节点（Text）。
     * 查询节点主要可以使用以下方法：
     *
     * Document new SAXReader().read(File file)    ——    读取XML文档
     * Element Document.getRootElement()    ——    获取XML文档的根元素节点
     * Iterator Element.nodeIterator()    ——    获取当前元素节点下的所有子节点
     * Iterator<Element> Element.elementIterator(元素名)    ——    指定名称的所有子元素节点
     * Iterator<Attribute> Element.attributeIterator()    ——    获取所有子属性节点
     * List<Element> Element.elements()    ——    获取所有子元素节点
     * List<Attribute> Element.attributes()    ——    获取所有子属性字节
     * Element Element.element(元素名)    ——    指定名称的第一个子元素节点
     * Attribute Element.attribute(属性名)    ——    获取指定名称的子属性节点
     * String Element.attributeValue(属性名)    ——    获取指定名称的子属性的属性值
     * String Attribute.getName()    ——    获取属性名称
     * String Attribute.getValue()    ——    获取属性值
     * String Element.getText()    ——    获取当前元素节点的子文本节点
     * String Element.elementText(元素名)    ——    获取当前元素节点的指定名称的子文本节点
     */
    @Test
    @SneakyThrows
    void analysisXML() {

        String property = System.getProperty("user.dir");
        File file = new File(property+"/src/main/resources/xml/student.xml");
        //创建一个XML 解析对象
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        Element rootElement = document.getRootElement();
        StringBuilder builder = new StringBuilder();
        recursion(rootElement,builder);
        System.out.println(builder.toString());
    }

    private void recursion(Element rootElement, StringBuilder builder) {
        builder.append("<");
        //解析元素节点
        builder.append(rootElement.getName());
        //解析属性节点
        List<Attribute> attributes = rootElement.attributes();
//        attributes.forEach( attribute -> {
//            builder.append(" ");
//            builder.append(attribute.getName());
//            builder.append("=");
//            builder.append(attribute.getValue());
//        });
        for (Attribute attribute : attributes) {
            builder.append(" ");
            builder.append(attribute.getName());
            builder.append("=");
            builder.append(attribute.getValue());
        }
        builder.append(">");
        //解析文本节点
        builder.append(rootElement.getText());
        List<Element> elements = rootElement.elements();
//        elements.forEach( element -> {
//            recursion(element,builder);
//        });
        for (Element element : elements) {
            recursion(element,builder);
        }
        builder.append("<" + rootElement.getName() + "/>\n");
    }

    /**
     * ①写出内容到xml文档
     *
     *     XMLWriter writer = new XMLWriter(OutputStream out, OutputFormat format)
     *
     *     writer.write(Document doc)
     *
     *     上面的OutputFormat对象可以由OutputFormat类的两个静态方法来生成：
     *
     * createPrettyPrint()    ——    生成的OutputFormat对象，使写出的XML文档整齐排列，适合开发环境使用
     * createCompactFormat()    ——    生成的OutputFormat对象，使写出的XML文档紧凑排列，适合生产环境使用
     *     ②生成文档或增加节点
     *
     * Document DocumentHelper.createDocument()    ——    生成一个新的XML Document对象
     * Element Element.addElement(元素节点名）    ——    增加一个子元素节点
     * Attribute Element.addAttribute(属性名，属性值)    ——    增加一个子属性节点
     *     ③修改节点
     *
     * Attribute.setValue(属性值)    ——    修改属性节点值
     * Attribute Element.addAttribute(同名属性名，属性值)    ——    修改同名的属性节点值
     * Element.setText(内容)    ——    修改文本节点内容
     *     ④删除节点
     *
     * Element.detach()    ——    删除元素节点
     * Attribute.detach()    ——    删除属性节点
     */
    @Test
    @SneakyThrows
    void createXML(){

        //创建一个XMLWriter 对象
        OutputFormat outputFormat = OutputFormat.createPrettyPrint();
        XMLWriter xmlWriter = new XMLWriter(new FileOutputStream("D:\\Idea_WorkSpace\\shen_root\\service\\service-until\\src\\main\\resources\\xml\\studentcopy.xml"), outputFormat);
        //生成一个新的Document对象
        Document root = DocumentHelper.createDocument();
        //新增节点
        Element students = root.addElement("Students");
        //增加两个Students元素节点
        Element student1 = students.addElement("student");
        Element student2 = students.addElement("student");
        //为这两个元素增加属性
        student1.addAttribute("id","001");
        student1.addAttribute("test1","测试01");
        student2.addAttribute("id","002");
        student2.addAttribute("test2","测试02");
        //分别增加name, age, gender,grade 元素子节点
        student1.addElement("name").setText("张三");
        student1.addElement("age").setText("19");
        student1.addElement("gender").setText("男");
        student1.addElement("grade").setText("计算机1班");
        student2.addElement("name").setText("李四");
        student2.addElement("age").setText("20");
        student2.addElement("gender").setText("男");
        student2.addElement("grade").setText("计算机2班");
        //将document写入磁盘
        xmlWriter.write(root);
        xmlWriter.close();

    }

    /**
     *  使用dom4j查询比较深的层次结构的节点时，比较麻烦，因此可以使用xPath技术快速获取所需的节点对象
     *  ①使用xPath的方法
     *
     * List<Node>  Document.selectNodes(xpath表达式)    ——    查询多个节点对象
     * Node  Document.selectSingleNode(xpath表达式)    ——    查询一个节点对象
     *     ②xPath表达式语法
     *
     * /     ——    绝对路径，表示从xml文档的根位置开始
     * //    ——    相对路径，表示不分任何层次结构的选择元素
     * *    ——    表示匹配所有元素
     * []    ——    条件，表示选择符合条件的元素
     * @    ——    属性，表示选择属性节点
     * and    ——    关系，表示条件的与关系
     * text()    ——    文本，表示选择文本内容
     */

    @Test
    @SneakyThrows
    void selectXML(){

        String property = System.getProperty("user.dir");
        File file = new File(property+"/src/main/resources/xml/student.xml");
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(file);

        //选择所有 Student 元素

//        List<Node> nodes = document.selectNodes("//Student");
//        nodes.forEach(node -> { System.out.println(node.getName() + node.getText()); });

        // 选择文本内容是"张三"的name元素

//        Element nameNode = (Element) document.selectSingleNode("//name[text()='张三']");
//        StringBuilder builder = new StringBuilder();
//        Element parent = nameNode.getParent();
//        String nodeInfo = getNodeInfo(parent, builder);
//        System.out.println("nodeInfo =>\n"+nodeInfo);

        //选则所有id属性的节点
//        List<Node> idNodes = document.selectNodes("//@id");
//        idNodes.forEach(idNode -> {
//            System.out.println(idNode.getName()+"=>"+idNode.getText());
//        });

        //选择 id 属性为002的student元素

//        Element element = (Element) document.selectSingleNode("//Student[@id='002']");
//        StringBuilder builder = new StringBuilder();
//        System.out.println(getNodeInfo(element, builder));

        //选择根元素节点的所有子节点
        Element rootElement = document.getRootElement();
        String rootName = rootElement.getName();
        List<Node> nodes =  document.selectNodes("/" + rootName + "/*");
        List<String> builders = new ArrayList<>();
        nodes.forEach(node -> {
            StringBuilder builder = new StringBuilder();
            Element element = (Element) node;
            String info = getNodeInfo(element, builder);
            builders.add(info);
        });
        System.out.println(builders);

    }

    //获取节点信息

    String getNodeInfo(Element node,StringBuilder builder){

        builder.append("<");
        builder.append(node.getName());
        List<Attribute> attributes = node.attributes();
        attributes.forEach(attr -> {
            builder.append(" "+attr.getName()+"="+attr.getValue());

        });
        builder.append(">");
//
        List<Element> elements = node.elements();
        elements.forEach(element -> {
            builder.append("\n");
            getNodeInfo(element,builder);
         });
        builder.append(node.getText());
        builder.append("</" + node.getName() + ">\n");
        return builder.toString();
    }

    //使用SAX 解析方式查询XML文档
    //SAX方式解析的原理是：在内存中加载一点，读取一点，处理一点。对内存要求比较低。
    /**
     *  核心的API类：
     *
     *         1、SAXParser.parse(File f, DefaultHandler dh)方法：解析XML文件
     *
     *             参数一File：表示读取的XMl文件
     *
     *             参数二DefaultHandler：SAX事件处理程序，包含SAX解析的主要逻辑。开发人员需要写一个DefaultHandler的子类，然后将其对象作为参数传入parse()方法。
     *
     *        2、 SAXParserFactory类，用于创建SAXParser对象，创建方式如下：
     *
     *             SAXParser parse = SAXParserFactory.newInstance().newSAXParser();
     *
     *        3、DefaultHandler类的主要方法：
     *
     *     void startDocument()    ——    在读到文档开始时调用
     *     void endDocument()    ——    在读到文档结束时调用
     *     void startElement(String uri, String localName, String qName, Attributes attributes)    ——    读到开始标签时调用
     *     void endElement(String uri, String localName, String qName)    ——    读到结束标签时调用
     *     characters(char[] ch, int start int length)    ——    读到文本内容时调用
     */

    @SneakyThrows
    @Test
    public void testSAXParser(){

        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        String property = System.getProperty("user.dir");
        File file = new File(property + "/src/main/resources/xml/student.xml");
        parser.parse(file,new MyDefaultHandler());
    }



}
class MyDefaultHandler extends DefaultHandler {
    @Override
    public void startDocument() throws SAXException {

        System.out.println("-----文档开始解析------");
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("-----文档解析结束------");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        System.out.print("<"+qName);
        for (int i = 0; i < attributes.getLength(); i++) {
            System.out.print(" "+attributes.getQName(i)+"="+attributes.getValue(i));
        }
        System.out.print(">");
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        System.out.print("</"+qName+">");
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        System.out.print(new String(ch, start,length));
    }
}