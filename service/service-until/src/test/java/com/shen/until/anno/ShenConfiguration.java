package com.shen.until.anno;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShenConfiguration {

    @Value("${shen.test.name}")
    private String test;

    @Bean
    @ConfigurationProperties(prefix = "shen.test")
    public Shen getShen(){

        Shen s = new Shen();
        return s;
    }

    @Test
    public void test(){
        System.out.println(test);
    }
}
