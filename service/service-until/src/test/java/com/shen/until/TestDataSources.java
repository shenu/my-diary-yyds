package com.shen.until;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

@SpringBootTest
public class TestDataSources {

    @Autowired
    private DataSource dataSource;

    @Test
    public void test1(){
        System.out.println(dataSource.getClass().getName());
    }

}
