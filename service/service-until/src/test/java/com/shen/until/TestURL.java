package com.shen.until;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shen.until.bean.Article;
import com.shen.until.service.ArticleService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
public class TestURL {

    @Autowired
    ArticleService articleService;
    @Test
    @SneakyThrows
    public void test1(){
        URL url = new URL("https://dl.stream.qqmusic.qq.com/C400002VVguJ3WnXb9.m4a?guid=5792054716&vkey=5B3F6344C4A60CF582A9C42A7BDB2BA0FFCBF2579D0187D3658F0BD63F0FE5D54503AB3746B78BE80F58ED009B95CA30C67D7859F746CDDD&uin=&fromtag=120002");
        URLConnection urlConnection = url.openConnection();
        InputStream is = urlConnection.getInputStream();
        byte[] buffer = new byte[1024];
        FileOutputStream outputStream = new FileOutputStream(new File("test.mp3"));
        int len;
        while ((len = is.read(buffer)) != -1){
            outputStream.write(buffer, 0,len);
        }
        outputStream.close();
        is.close();
    }

    @Test
    public void test(){
        String property = System.getProperty("user.dir");
        File file = new File(property);
        file = file.getParentFile().getParentFile();
        Map<String,List<String>> map = new HashMap<>();
        Map<String, List<String>> listMap = getContainssWordFile(map, file, "数据导出");
        listMap.forEach((k,v)->{
            if (map.get(k) != null){
                map.get(k).forEach(l->{
                    File file1 = new File(k+l);
                    boolean delete = file1.delete();
                    System.out.println(delete);
                    System.out.println(k+"\\"+l);
                });
            }
        });
    }

    @Test
    public void test2(){
        String property = System.getProperty("user.dir");
        System.out.println(property);

        File file = new File(property);
        file = file.getParentFile().getParentFile();
        Map<String,List<String>> map = new HashMap<>();
        Map<String, List<String>> listMap = getContainssWordFile(map, file, "数据导出");
        listMap.forEach((k,v)->{
            if (map.get(k) != null){
                map.get(k).forEach(l->{
                    File file1 = new File(k+l);
                    System.out.println(k+"\\"+l);
                });
            }
        });

    }

    //递归查找包含指定名称的文件
    public Map<String,List<String>> getContainssWordFile(Map<String,List<String>> map, File file,
                                                         final String targetStr) {

        if (file.isDirectory()){
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                getContainssWordFile(map, files[i], targetStr);
            }
        }else{
            if (file.getName().contains(targetStr)){
                String path = file.getPath();
                if (map.get(path) != null){
                    map.get(path).add(file.getName());
                }else{
                    List<String> list = new ArrayList<>();
                    list.add(file.getName());
                    map.put(path, list);
                }
            }
        }

        return map;
    }


    @Test
    public void testPage(){
        Page<Article> page = new Page<>(1,5);
        articleService.page(page,null);
        System.out.println(page.getTotal());
        System.out.println(page.getRecords());
    }


}
