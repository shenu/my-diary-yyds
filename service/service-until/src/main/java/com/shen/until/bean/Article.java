package com.shen.until.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("svi_article")
public class Article {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String title;
    @TableField(value = "content")
    private String contentMsg;
    @TableField(value = "category_id")
    private String categoryId;
    @TableField(value = "label_list")
    private String LabelList;
    @TableField(value = "status")   //1-新发布；2-进行中；3-已完成；4-取消；
    private Integer status;
    @TableField(value = "created_user")
    private String createdUser;
    @TableField(value = "created_time",fill = FieldFill.INSERT)
    private Date createdTime;
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;
    
}
