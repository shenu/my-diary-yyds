package com.shen.until.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.until.bean.Article;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Map;

public interface ArticleService extends IService<Article> {
    //数据导出
    File dataExportToExcel(Map<String, Object> conditionMap);

    //数据导入
    boolean dataImportFromExcel(MultipartFile multipartFile);
}
