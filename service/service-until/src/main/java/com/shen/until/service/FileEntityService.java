package com.shen.until.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.until.bean.FileEntity;

public interface FileEntityService extends IService<FileEntity> {
}
