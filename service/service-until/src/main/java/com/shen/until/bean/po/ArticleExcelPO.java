package com.shen.until.bean.po;

import lombok.Data;

@Data
public class ArticleExcelPO {
    private Integer id;
    private String title;
    private String contentMsg;
    private String categoryId;
    private String LabelList;
    private Integer status;
    private String createdUser;
}
