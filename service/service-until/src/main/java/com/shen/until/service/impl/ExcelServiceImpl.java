package com.shen.until.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shen.until.bean.xslx.OrderExcel;
import com.shen.until.mapper.ExcelMapper;
import com.shen.until.service.ExcelService;
import org.springframework.stereotype.Service;

@Service
public class ExcelServiceImpl extends ServiceImpl<ExcelMapper, OrderExcel> implements ExcelService {
}
