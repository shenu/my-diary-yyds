package com.shen.until.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shen.common.utils.DateUtil;
import com.shen.until.bean.Article;
import com.shen.until.bean.po.ArticleExcelPO;
import com.shen.until.service.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("article")
@Slf4j
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping("goto-index")
    public String gotoArticleIndex(){

        return "article/index";
    }


    @GetMapping("goto-list")
    public String gotoArticleList(){

        return "article/list";
    }

    //列表
    @GetMapping("getArticleList")
    @ResponseBody
    public List<Article> getArticleList(){

        List<Article> list = articleService.list(null);
        return list;
    }
    //带分页数据列表
    //搜索参数说明 ：searchVal 搜索内容
    //             searchLabel搜索标签  1 =》内容 ；2 =》标签 ；3 =》全匹配
    @PostMapping("getArticleListPage/{current}/{limit}")
    @ResponseBody
    public Map<String, Object> getArticleListPage(@PathVariable("current") int current,
                                            @PathVariable("limit") int limit,
                                            @RequestBody Map<String, Object> params){
        Page<Article> page = new Page<>(current,limit);
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        //参数校验

//        StringUtils.isEmpty(params.get("status").toString())  //无法判断空字符串
        if (params.size() > 0 && params.get("status") != null
                && params.get("status").toString()!= null && !"".equals(params.get("status").toString())){
            Integer status = Integer.valueOf (params.get("status").toString());
            queryWrapper.eq("status",status);
        }
        //校验是否包含条件参数   params.containsKey("searchLabel")
        if (params.containsKey("searchVal") && params.get("searchVal") != null && params.get("searchVal").toString() != null && !"".equals(params.get("searchVal").toString())){
            String searchVal = "";
            System.out.println("searchVal"+params.get("searchVal"));
            if (params.containsKey("searchLabel") && params.get("searchLabel") != null && params.get("searchLabel").toString() != null && !"".equals(params.get("searchLabel").toString())){
                //既包含searchVal  同时又包含searchLabel
                searchVal = params.get("searchVal").toString();
                System.out.println("searchLabel"+params.get("searchLabel"));
                Integer searchLabel = Integer.valueOf(params.get("searchLabel").toString());
                switch (searchLabel){
                    case 1 : //搜索内容包含
                        queryWrapper.like("content",searchVal);
                        break;
                    case 2 : //搜索标签包含
                        queryWrapper.like("label_list",searchVal);
                        break;
                    case 3 : //同时搜索
                        queryWrapper.like("label_list",searchVal);
                        queryWrapper.like("content",searchVal);
                        break;
                }

            }else if (!params.containsKey("searchLabel")){
                //只包含searchVal 默认全包含
                searchVal = params.get("searchVal").toString();
                queryWrapper.like("label_list",searchVal);
                queryWrapper.like("content",searchVal);

            }
        }
        System.out.println(articleService.list(queryWrapper));
//        articleService.pageMaps()
        articleService.page(page,queryWrapper);
        long total = page.getTotal();
        long pages = page.getPages();
        List<Article> records = page.getRecords();
        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("pages",pages);
        map.put("records",records);
        log.info("total"+total);
        log.info("pages"+pages);
        log.info("records",records);
        return map;
    }

    //保存
    @PostMapping("saveArticle")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Map<String,Object> saveArticle(@RequestBody Map<String,Object> params){

        Map<String,Object> map = new HashMap<>();
        Article article = new Article();
        article.setTitle(params.get("title").toString());
        article.setContentMsg(params.get("contentMsg").toString());
        article.setLabelList(params.get("labels").toString());
        article.setCreatedUser("shen");
        article.setStatus(1);
        articleService.save(article);
        System.out.println(params.toString());
        map.put("code",200);
        map.put("msg","success");
        return map;
    }

    //删除
    @DeleteMapping("article/{id}")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public String delArticle(@PathVariable Integer id){

        System.out.println(id);
        articleService.removeById(id);
        return "success";
    }

    //修改页面数据回显
    @GetMapping("uploadArticle/{id}")
    @ResponseBody
    public Article gotoUpdatePage(@PathVariable Integer id){

        Article article = articleService.getById(id);
        return article;
    }
    //实现修改
    @PutMapping("article/{id}")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> updateArticle(@RequestBody Map<String,Object> params,@PathVariable Integer id){

        Map<String, Object> map = new HashMap<>();
        Article article = articleService.getById(id);
        System.out.println(params);
        article.setTitle(params.get("title").toString());
        article.setContentMsg(params.get("contentMsg").toString());
        article.setLabelList(params.get("labels").toString());
        articleService.updateById(article);
        map.put("code",200);
        map.put("msg","success");
        return map;
    }

    @PostMapping("article/{id}")
    @ResponseBody
    public Map<String, Object> updateArticleStatus(@RequestBody Map<String,Object> params,@PathVariable Integer id){
        Map<String, Object> map = new HashMap<>();
        Article article = articleService.getById(id);
        Integer status = null;
        System.out.println(params);
        if (params.size() > 0 && params.get("status") != null
                && params.get("status").toString()!= null && !"".equals(params.get("status").toString())){
            status = Integer.valueOf (params.get("status").toString());
        }
        article.setStatus(status);
        articleService.updateById(article);
        map.put("code",200);
        map.put("msg","success");
        return map;
    }

    //数据导出
    @GetMapping("dataExportToExcel")
    @ResponseBody
    public void dataExportToExcel(HttpServletRequest request, HttpServletResponse response){



        String fileName = "数据导出-"+DateUtil.fomatDateToString(new Date())+".xlsx";
        String userAgent = request.getHeader("User-Agent");//获取浏览器名（IE/Chome/firefox）
        if(userAgent.contains("MSIE")||userAgent.contains("Trident")) {//针对IE或IE为内核的浏览器
            try {
                fileName=java.net.URLEncoder.encode(fileName,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }else {
            try {
                fileName=new String(fileName.getBytes("UTF-8"),"ISO-8859-1");//谷歌控制版本
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        //导出数据的条件
        Map<String, Object> conditionMap = new HashMap<>();
        File file = articleService.dataExportToExcel(conditionMap);
        int len = 0;
        FileInputStream fileInputStream = null;
        OutputStream outputStream = null;
        byte[] data = new byte[1024];
        try {
            fileInputStream = new FileInputStream(file);
            outputStream = response.getOutputStream();

            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            int read;
            while ((read = fileInputStream.read(data)) != -1) {
                outputStream.write(data, 0, read);
            }
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    //数据导入
    @PostMapping("dataImportFromExcel")
    @ResponseBody
    public String dataImportFromExcel(MultipartFile multipartFile){
        boolean b = articleService.dataImportFromExcel(multipartFile);
        if (b) {
            return "success";
        }else{
            return "failure";
        }
    }

}
