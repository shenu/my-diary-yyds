package com.shen.until.service;

import com.shen.until.bean.User;

import java.util.List;

/**
 * @author
 * @param("")
 * @date
 *
 */
public interface CacheService {

    public List<User> userList();
}
