package com.shen.until.scheduling;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@EnableScheduling
@Component
public class FileDealScheduling {

    //导出会产生文件
    //定时删除该文件
    @Scheduled(cron = "* */10 * * * *")
    public void delExportTempFile(){
        String property = System.getProperty("user.dir");
        System.out.println(property);

        File file = new File(property);
        file = file.getParentFile().getParentFile();
        Map<String,List<String>> map = new HashMap<>();
        Map<String, List<String>> listMap = getContainssWordFile(map, file, "数据导出");
        listMap.forEach((k,v)->{
            if (map.get(k) != null){
                map.get(k).forEach(l->{
                    File file1 = new File(k+l);
                    file1.delete();
                    System.out.println(k+"\\"+l);
                });
            }
        });

    }

    //递归查找包含指定名称的文件
    public Map<String, List<String>> getContainssWordFile(Map<String,List<String>> map, File file,
                                                          final String targetStr) {

        if (file.isDirectory()){
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                getContainssWordFile(map, files[i], targetStr);
            }
        }else{
            if (file.getName().contains(targetStr)){
                String path = file.getPath();
                if (map.get(path) != null){
                    map.get(path).add(file.getName());
                }else{
                    List<String> list = new ArrayList<>();
                    list.add(file.getName());
                    map.put(path, list);
                }
            }
        }

        return map;
    }

    //定时清理文件上传下的文件


}
