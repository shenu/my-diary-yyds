package com.shen.until.infocollect.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auther
 * @createdate 2023/5/25
 * @description
 */
// 标识此注解可以标注在 类，方法上
@Target({ElementType.METHOD,ElementType.TYPE})

// 运行时生效
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginAOPAnnotation {
    // 接收参数
    String desc() default "";
}
