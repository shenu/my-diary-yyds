package com.shen.until.controller.test;

import com.shen.until.bean.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

//    @Value("${shen.test.name}")
    @Value("#{getUser.id}")
    private String name;


    @GetMapping("test-name")
    public String testGetValue(){

        return name;
    }

    @Bean
    public User getUser(){
        User user = new User();

        user.setId("123");
        user.setName("shen");

        return user;
    }


}
