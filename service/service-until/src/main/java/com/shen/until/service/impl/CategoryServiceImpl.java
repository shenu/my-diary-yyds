package com.shen.until.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shen.until.bean.Category;
import com.shen.until.mapper.CategoryMapper;
import com.shen.until.service.CategoryService;
import org.springframework.stereotype.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
}
