package com.shen.until.bean.vo;

import lombok.Data;

import java.util.Date;

@Data
public class UserVo {
    private String id;

    private String userName;

    private String openId;

    private String nickName;

    private String phone;

    private String avatar;

    private String sign;

    private Integer sex;

    private Integer identity;

    private boolean isDisable;

    private Date gmtCreate;

    private Date gmtModified;

}
