package com.shen.until.easyexcellistener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.shen.until.bean.Article;
import com.shen.until.bean.po.ArticleExcelPO;
import com.shen.until.service.ArticleService;
import com.shen.until.service.impl.ArticleServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
public class ArticleListener implements ReadListener<ArticleExcelPO> {

    private ArticleService articleService = new ArticleServiceImpl();

    //每隔5条存储数据库，实际使用中可以100条，然后清理list，方便内存回收
    private static final int BATCH_COUNT = 1000;
    //缓存数据
    private List<Article> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    @Override
    public void invoke(ArticleExcelPO articleExcelPO, AnalysisContext analysisContext) {
        log.info("读取记录如下："+articleExcelPO);
        Article article = new Article();
        BeanUtils.copyProperties(articleExcelPO,article);
        cachedDataList.add(article);
        if (cachedDataList.size() >= BATCH_COUNT){
            log.info("存入数据库");
            articleService.saveBatch(cachedDataList);
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

        log.info("保存最后的数据");
        articleService.saveBatch(cachedDataList);
    }
}
