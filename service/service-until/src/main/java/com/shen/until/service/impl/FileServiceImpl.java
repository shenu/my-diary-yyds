package com.shen.until.service.impl;

import com.alibaba.excel.EasyExcel;
import com.shen.until.bean.FileEntity;
import com.shen.until.bean.xslx.OrderExcel;
import com.shen.until.easyexcellistener.OrderListener;
import com.shen.until.mapper.FileMapper;
import com.shen.until.service.FileEntityService;
import com.shen.until.service.FileService;
import com.shen.until.unitl.FileUntil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {

    @Resource
    private FileMapper fileMapper;


    @SneakyThrows
    @Override
    public String upload(MultipartFile file) {
        String filename = file.getOriginalFilename();
        String extName = filename.substring(filename.lastIndexOf("."), filename.length());
        //进行文件的copy
//        判断文件夹是否存在
        File uploadFileDir = new File("c:\\upload");
        if(!uploadFileDir.exists()){
            uploadFileDir.mkdirs();
        }
        FileOutputStream fileOutputStream = null;
        File copyFile = null;
        try {
            //获取文件流对象
            InputStream inputStream = file.getInputStream();
//        创建新的文件名 随机字符串+日期
            SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd");
            String newFileName = UUID.randomUUID().toString()+"-" + dataFormat.format(new Date())+extName;
            copyFile = new File(uploadFileDir +"\\"+ newFileName);
            fileOutputStream = new FileOutputStream(copyFile);
            //缓冲区
            byte[] buffer= new byte[1024 * 5];
            int len;
            while ( (len = inputStream.read(buffer)) > 0){
                fileOutputStream.write(buffer,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileOutputStream.close();
        }


        return copyFile.getAbsolutePath();
    }

    @Override
    public String excelDeal(MultipartFile file) {
        File toFile = FileUntil.multipartFileToFile(file);
        analysisExcel(toFile);
        return "success";
    }

    //xls 或者xlsx文件 需要解析字段入库
    public void analysisExcel(File file){

        EasyExcel.read(file,OrderExcel.class ,new OrderListener()).sheet().doRead();
    }

    @Override
    public void getFileZipOut(List<String> ids,String userid,String loadFilePath) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        File userLoadPath = new File(loadFilePath + "/" + userid);
        if (userLoadPath == null || !userLoadPath.exists()){
            userLoadPath.mkdirs();
        }
        String parent = loadFilePath + "/" + userid + "/"+ sdf.format(new Date()) + "/" +UUID.randomUUID().toString().replaceAll("-","") ;
        //获取文件根据id
        for (String id : ids) {
            FileEntity file = fileMapper.selectById(id);
            String fileAllPath = file.getFileAllPath();
            File fileTar = new File(fileAllPath);
            File out = new File(parent);
            FileInputStream is = null;
            FileOutputStream os = null;
            if (!out.exists()){
                out.mkdirs();
            }
            try {
                is = new FileInputStream(fileTar);
                os = new FileOutputStream(out + "/" + fileTar.getName());
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0){
                    os.write(buffer,0,length);
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                try{
                    is.close();
                    os.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }
    }



    //将MultipartFileToFile 转化为file
//    public File multipartFileToFile(MultipartFile multipartFile){
//
//        // 获取文件名
//        String filename = multipartFile.getOriginalFilename();
//        // 获取文件后缀
//        String prefix = filename.substring(filename.lastIndexOf("."));
//        try {
//            File tempFile = File.createTempFile(filename, prefix);
//            multipartFile.transferTo(tempFile);
//            return tempFile;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//
//    }
}
