package com.shen.until.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "svi_category")
public class Category {
    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    @TableField(value = "category_id")
    private String categoryId;
    @TableField(value = "category_par_id")
    private String categoryParId;
    @TableField(value = "category_name")
    private String categoryName;
}
