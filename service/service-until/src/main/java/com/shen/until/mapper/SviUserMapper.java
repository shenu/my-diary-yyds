package com.shen.until.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shen.until.bean.SviUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
public interface SviUserMapper extends BaseMapper<SviUser> {

}
