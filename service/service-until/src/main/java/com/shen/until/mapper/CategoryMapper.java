package com.shen.until.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shen.until.bean.Category;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
