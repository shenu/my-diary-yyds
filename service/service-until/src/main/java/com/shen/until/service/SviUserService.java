package com.shen.until.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.until.bean.SviUser;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
public interface SviUserService extends IService<SviUser> {

//    查询所有用户带分页
    Map<String,Object> getUserListByPage(Integer current, Integer limit);

    //增加用户
    Integer addUser(SviUser user);

    //根据用户名获取用户信息
    SviUser getUserByUserName(String userName);
}
