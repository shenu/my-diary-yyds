package com.shen.until.bean.xslx;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@ToString
@TableName("order_import")
public class OrderExcel {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String orderId;
    private String userName;
    private String createdTime;
    private String updateTime;

}
