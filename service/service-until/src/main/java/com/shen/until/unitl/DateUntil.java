package com.shen.until.unitl;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUntil {

    public static String getDateToString(Date date){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return format.format(date);
    }
}
