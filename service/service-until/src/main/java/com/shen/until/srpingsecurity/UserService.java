package com.shen.until.srpingsecurity;


import com.shen.until.bean.SviUser;

public interface UserService {

    SviUser getUserByName(String userName);
}
