package com.shen.until.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "svi_label")
public class Label {
    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    @TableField(value = "label_id")
    private String labelId;
    @TableField(value = "label_name")
    private String labelName;

    public Label(String labelId, String labelName) {
        this.labelId = labelId;
        this.labelName = labelName;
    }
}
