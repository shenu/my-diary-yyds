package com.shen.until.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shen.until.bean.FileEntity;

public interface FileMapper extends BaseMapper<FileEntity> {
}
