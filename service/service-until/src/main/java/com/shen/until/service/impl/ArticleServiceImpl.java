package com.shen.until.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shen.until.bean.Article;
import com.shen.until.bean.po.ArticleExcelPO;
import com.shen.until.easyexcellistener.ArticleListener;
import com.shen.until.mapper.ArticleMapper;
import com.shen.until.service.ArticleService;
import com.shen.until.unitl.FileUntil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper,Article> implements ArticleService {

    //数据 导出
    @Override
    public File dataExportToExcel(Map<String, Object> conditionMap) {
        List<Article> articles = baseMapper.selectList(null);
        String excelName_suffix = ".xlsx";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //这里需要指定用那个class 去写，然后写到第一个sheet，名字为模板，然后文件流会自动关闭
        String fileName = "数据导出"+dateFormat.format(new Date())+excelName_suffix;
        File file = new File(fileName);
        List<ArticleExcelPO> articleExcels = new ArrayList<>();
        articles.forEach(article -> {
            ArticleExcelPO excel = new ArticleExcelPO();
            BeanUtils.copyProperties(article,excel);
            articleExcels.add(excel);
        });
        EasyExcel.write(fileName,ArticleExcelPO.class).sheet("数据导出").doWrite(articleExcels);
        return file;
    }

    //数据导入
    @Override
    public boolean dataImportFromExcel(MultipartFile multipartFile) {

        File file = FileUntil.multipartFileToFile(multipartFile);
        EasyExcel.read(file,ArticleExcelPO.class,new ArticleListener()).sheet().doRead();
        return true;
    }


}
