package com.shen.until.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.until.bean.xslx.OrderExcel;

public interface ExcelService extends IService<OrderExcel> {
}
