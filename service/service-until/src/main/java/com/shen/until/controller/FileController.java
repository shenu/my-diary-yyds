package com.shen.until.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shen.until.bean.FileEntity;
import com.shen.until.service.FileEntityService;
import com.shen.until.service.FileService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@Controller
@RequestMapping("file")
@CrossOrigin
public class FileController {

    @Value("${upload.file.path}")
    String uploadFilePath;

    @Value("${load.file.path}")
    String loadFilePath;

    @Autowired
    private FileService fileService;

    @Autowired
    private FileEntityService fileEntityService;

    //展现文件上传页面
    @GetMapping("goto-upload-page")
    public String goUploadPage(){

        return "file/upload";
    }
    //获取展示数据
    @GetMapping("getFileList")
    @ResponseBody
    public List<FileEntity> getFilesList(){

        QueryWrapper<FileEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("id");
        List<FileEntity> list = fileEntityService.list(wrapper);
        return list;
    }

    //获取文件列表 分页
    @GetMapping("getFilesByPage/{current}/{limit}")
    @ResponseBody
    public Map<String, Object> getFilesPage(@PathVariable Integer current,
                                            @PathVariable Integer limit){
        Map<String, Object> map = new HashMap<>();
        Page<FileEntity> page = new Page<>(current,limit);
        fileEntityService.page(page,null);
        long total = page.getTotal();
        long pages = page.getPages();
        List<FileEntity> records = page.getRecords();
        map.put("total",total);
        map.put("pages",pages);
        map.put("records",records);
        return map;
    }

    ///文件上传
    @SneakyThrows
    @PostMapping("upload-file")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public String fileUpload(MultipartFile multipartFile){

        String newFileName = null;
        String originalFilename = multipartFile.getOriginalFilename();
        FileEntity fileEntity = new FileEntity();

        //文件复制 ---上传
        newFileName = fileService.upload(multipartFile);
        fileEntity.setFileName(originalFilename);
        fileEntity.setFileSize(Double.valueOf(multipartFile.getSize()));
        fileEntity.setFileNewName(newFileName.substring(newFileName.lastIndexOf("\\")+1));
        fileEntity.setFileAllPath(newFileName);
        fileEntityService.save(fileEntity);
        //excel 解析
//        String deal = fileService.excelDeal(file);
        return newFileName;
    }

    //上传excel并处理
    @PostMapping("excel-upload-deal")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public String excelUploadDeal(MultipartFile multipartFile){

        String newName = fileService.upload(multipartFile);
        String deal = fileService.excelDeal(multipartFile);
        Map<String,String> map = new HashMap<>();
        map.put("newName",newName);
        map.put("status","success");
        JSONObject.toJSONString(map);
        return JSONObject.toJSONString(map);
    }

    //展现已上传文件列表
//    @GetMapping("goto-file-list")
    @RequestMapping("goto-file-list")
    public String getFileList(){
        return "/file/list";
    }

    //下载文件
    @PostMapping("file-download")
//    @GetMapping("file-download")
    public void fileDownload(HttpServletResponse response, HttpServletRequest request){

        String parameter = request.getParameter("fileName");
        System.out.println(parameter);
        File file = new File(parameter);
        String fileName = file.getName();
        FileInputStream fileInputStream = null;
        OutputStream outputStream = null;
        byte[] data = new byte[1024];
        try {
            fileInputStream = new FileInputStream(file);
            outputStream = response.getOutputStream();
//            response.setHeader("Pragma", "No-cache");
//            response.setHeader("Cache-Control", "No-cache");
//            response.setDateHeader("Expires", 0);
//            response.setContentType("application/x-msdownload");
//            response.setContentType("application/octet-stream");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");
            int read;
            while ((read = fileInputStream.read(data)) != -1){
                outputStream.write(data,0,read);
            }
            outputStream.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //下载文件
    @PostMapping("file-download2")
    public void fileDownload2(HttpServletResponse response, HttpServletRequest request){

        String parameter = request.getParameter("fileName");
        System.out.println(parameter);
        File file = new File(parameter);
        String fileName = file.getName();
        FileInputStream fileInputStream = null;
        OutputStream outputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            outputStream = response.getOutputStream();
//            response.setHeader("Pragma", "No-cache");
//            response.setHeader("Cache-Control", "No-cache");
//            response.setDateHeader("Expires", 0);
//            response.setContentType("application/x-msdownload");
//            response.setContentType("application/octet-stream");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");
            outputStream.write(fileInputStream.read());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //文件批量下载
    @PostMapping("file-batchLoad")
    public void batchFileLoad(@RequestBody Map<String,List<String>> param,HttpServletResponse response,HttpServletRequest request){
        System.out.println(param);
        Cookie[] cookies = request.getCookies();
        String userid = null;
        for (Cookie cookie : cookies) {
            if ("userID".equals(cookie.getName())){
                userid = cookie.getValue();
            }
        }
        List<String> ids = param.get("params");
        fileService.getFileZipOut(ids,userid,loadFilePath);
    }

    //文件删除
    @DeleteMapping("file/{id}")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> deleteFile(@PathVariable("id") Integer id) {

        Map<String, Object> map = new HashMap<>();
        //查询是否存在记录
        FileEntity fileEntity = fileEntityService.getById(id);
        //删除记录
        boolean b = fileEntityService.removeById(id);
        //删除所在文件
        if (b){
            File file = new File(fileEntity.getFileAllPath());
            if (file.exists() && file.isFile()){
                file.delete();
            }
        }
        map.put("code",200);
        map.put("msg","success");
        return map;
    }

    //文件批量删除
    @DeleteMapping("batchDelFileById")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> batchDelFileById(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        String parameter = request.getParameter("ids");
        String replaceStr = parameter.replaceAll("\\[","").replaceAll("]","");
        String[] split = replaceStr.split(",");
        System.out.println(split.toString());
        List<FileEntity> list = (List<FileEntity>) fileEntityService.listByIds(Arrays.asList(split));
        if (list.size() == split.length){
            boolean b = fileEntityService.removeByIds(Arrays.asList(split));
            if (b){
                list.forEach(fileEntity -> {
                   File file = new File(fileEntity.getFileAllPath());
                   if (file.exists() && file.isFile()){
                       file.delete();
                   }
                });
            }
        }
        map.put("code",200);
        map.put("msg","success");
        return map;
    }


}
