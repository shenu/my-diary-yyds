package com.shen.until.controller;

import com.shen.until.bean.Label;
import com.shen.until.service.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("label")
public class LabelController {

    @Autowired
    private LabelService labelService;

    @GetMapping("getLabelList")
    public List<Label> getLabelList (){
        List<Label> list = labelService.list(null);
        return list;
    }
    @PostMapping("saveLabel")
    public Map<String,Object> saveLabel(){
        Map<String, Object> map = new HashMap<>();
        List<Label> list = new ArrayList<>();
        Label label1 = new Label(UUID.randomUUID().toString(),"URL");
        list.add(label1);
        labelService.saveBatch(list);
        return map;
    }

}
