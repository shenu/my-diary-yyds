package com.shen.until.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {

    //文件上传
    public String upload(MultipartFile file);
    //excel 解析
    public String excelDeal(MultipartFile file);

    //批量下载文件将文件打包为整个压缩包进行下载
    void getFileZipOut(List<String> ids,String userid,String loadFilePath);
}
