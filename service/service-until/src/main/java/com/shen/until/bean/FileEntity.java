package com.shen.until.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "myfile")
public class FileEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String fileName;
    private String fileNewName;
    private String fileAllPath;
    private Double fileSize;
    private String description;
}
