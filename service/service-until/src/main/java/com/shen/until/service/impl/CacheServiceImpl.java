package com.shen.until.service.impl;

import com.shen.until.bean.User;
import com.shen.until.service.CacheService;
import lombok.SneakyThrows;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CacheServiceImpl implements CacheService {


    @SneakyThrows
    @Override
    @Cacheable("users")
    public List<User> userList() {
        List<User> userList = new ArrayList<>();
        Thread.currentThread().sleep(5000);
        for (int i = 1; i <= 10; i++) {
            User user = new User(UUID.randomUUID().toString(), "test" + i);
            userList.add(user);
        }
        return userList;
    }


}
