package com.shen.until.infocollect;

import com.shen.until.infocollect.anno.LoginAOPAnnotation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Auther
 * @createdate 2023/5/25
 * @description
 */
@Aspect  // 申明切面
@Component
public class LoginCollectAOP {

    // * com.shen.until.controller.IndexController.*()
    // 方式一 ： 直接设置对应路径下的类设置切点
//    @Pointcut("execution(* com.shen.until.controller.IndexController.*(..))")
    // 方式二 使用注解实现，进行点对点
    @Pointcut(value = "@annotation(com.shen.until.infocollect.anno.LoginAOPAnnotation)")
    public void insertLoginInfo(){
        System.out.println("**** 进入切点 ****");

    }

    /**
     * *****************************************************************************
     * 方式一
     */
    //前置通知
//    @Before("insertLoginInfo()")
//    public void deBefore(JoinPoint jp) throws Throwable {
//        // 接收到请求，记录请求内容
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        HttpServletRequest request = attributes.getRequest();
//        // 记录下请求内容
//        System.out.println("URL : " + request.getRequestURL().toString());
//        System.out.println("HTTP_METHOD : " + request.getMethod());
//        System.out.println("CLASS_METHOD : " + jp);
//        System.out.println("ARGS : " + Arrays.toString(jp.getArgs()));
//        System.out.println("前置通知：方法执行前执行...");
//    }
//    //返回通知
//    @AfterReturning(returning = "ret", pointcut = "insertLoginInfo()")
//    public void doAfterReturning(Object ret) throws Throwable {
//        // 处理完请求，返回内容
//        System.out.println("返回通知：方法的返回值 : " + ret);
//    }
//
//    //异常通知
//    @AfterThrowing(throwing = "ex", pointcut = "insertLoginInfo()")
//    public void throwss(JoinPoint jp, Exception ex) {
//        System.out.println("异常通知：方法异常时执行.....");
//        System.out.println("产生异常的方法：" + jp);
//        System.out.println("异常种类：" + ex);
//    }
//
//    //后置通知
//    @After("insertLoginInfo()")
//    public void after(JoinPoint jp){
//        System.out.println("后置通知：最后且一定执行.....");
//    }

    /**
     * *****************************************************************************
     * 方式二
     */
    //前置通知
    //注意：获取注解中的属性时，@annotation()中的参数要和deBefore方法中的参数写法一样，即myLogAnnotation
    @Before("insertLoginInfo() && @annotation(loginAOPAnnotation)")
    public void deBefore(JoinPoint jp, LoginAOPAnnotation loginAOPAnnotation) throws Throwable {
        System.out.println("前置通知：方法执行前执行...");
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 获取注解中的属性
        System.out.println("deBefore===========>" + loginAOPAnnotation.desc());
        // 记录下请求内容
        System.out.println("URL : " + request.getRequestURL().toString());
    }
    //返回通知
    @AfterReturning(returning = "ret", pointcut = "insertLoginInfo() && @annotation(loginAOPAnnotation)")
    public void doAfterReturning(Object ret, LoginAOPAnnotation loginAOPAnnotation) throws Throwable {
        // 获取注解中的属性
        System.out.println("doAfterReturning===========>" + loginAOPAnnotation.desc());
        // 处理完请求，返回内容
        System.out.println("返回通知：方法的返回值 : " + ret);
    }

    //异常通知
    @AfterThrowing(throwing = "ex", pointcut = "insertLoginInfo() && @annotation(loginAOPAnnotation)")
    public void throwss(JoinPoint jp,Exception ex, LoginAOPAnnotation loginAOPAnnotation){
        // 获取注解中的属性
        System.out.println("throwss===========>" + loginAOPAnnotation.desc());
        System.out.println("异常通知：方法异常时执行.....");
        System.out.println("产生异常的方法："+jp);
        System.out.println("异常种类："+ex);
    }

    //后置通知
    @After("insertLoginInfo() && @annotation(loginAOPAnnotation)")
    public void after(JoinPoint jp, LoginAOPAnnotation loginAOPAnnotation){
        // 获取注解中的属性
        System.out.println("after===========>" + loginAOPAnnotation.desc());
        System.out.println("后置通知：最后且一定执行.....");
    }
}
