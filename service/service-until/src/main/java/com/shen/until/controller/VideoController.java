package com.shen.until.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("video")
public class VideoController {

    @RequestMapping("getVideoSource")
    public void getVideoSource(HttpServletResponse response){

        String path = "D:\\KuGou\\王力宏 - 天地龙鳞.mp3";
        File file = new File(path);
        ServletOutputStream responseOutputStream = null;
        String fileName = UUID.randomUUID().toString()+".mp3";
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            responseOutputStream = response.getOutputStream();
            byte[] data = new byte[1024];

            //全文件类型（传什么文件返回什么文件）
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("apllication/x-msdownload");
            response.setHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");
            response.setHeader("Accept-Ranges", "bytes");
            int read;
            while ((read = fileInputStream.read(data)) != -1){
                responseOutputStream.write(data,0,read);
            }
            responseOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                responseOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @RequestMapping("getVideoSourceAll")
    public void getVideoSourceAll(HttpServletResponse response){
        String path = "C:\\Windows\\Media\\Alarm01.wav";
//        String path = "D:\\KuGou\\王力宏 - 天地龙鳞.mp3";
        File file = new File(path);
        ServletOutputStream responseOutputStream = null;
        String fileName = UUID.randomUUID().toString()+".wav";
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            responseOutputStream = response.getOutputStream();
            System.out.println(fileInputStream.available());
            byte[] data = new byte[fileInputStream.available()];
            //全文件类型（传什么文件返回什么文件）
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("apllication/x-msdownload");
            response.setHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");
            response.setHeader("Accept-Ranges", "bytes");
            int read = fileInputStream.read(data);
            responseOutputStream.write(data,0,read);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                responseOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

}
