package com.shen.until.config;

import com.alibaba.fastjson.JSONObject;
import com.shen.until.bean.SviUser;
import com.shen.until.service.SviUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;

    @Autowired
    private SviUserService sviUserService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.headers().frameOptions().sameOrigin();    //同源可嵌入
        http.csrf().disable();
        http.authorizeRequests().antMatchers("/login/**","/images/**","/login").permitAll();
        http.authorizeRequests().antMatchers("/file/**","/article/**").hasRole("USER");
        http.logout().logoutUrl("/logout-ToLogin");
        //设置默认首页
        http.formLogin().loginPage("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .failureUrl("/error")
                .loginProcessingUrl("/login/loginToIndex")
                .defaultSuccessUrl("/success-login");
        http.formLogin()
                .failureHandler(new AuthenticationFailureHandler(){
                    @Override
                    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {

                        httpServletResponse.setCharacterEncoding("utf-8");
                        httpServletResponse.setHeader("Content-Type","text/html;charset=UTF-8");
                        Map<String,Object> map = new HashMap<>();
                        map.put("code",400);
                        map.put("url","");
                        if ("passwd Error".equals(e.getMessage())){
                            map.put("msg","密码错误");
                            httpServletResponse.getWriter().write(JSONObject.toJSONString(map));
                            return;
                        }
                        map.put("msg","用户名不存在");
                        httpServletResponse.getWriter().write(JSONObject.toJSONString(map));
                        return;

                    }
        });
        http.formLogin().successHandler(successHandler());
        super.configure(http);

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);
        //基于database认证
        auth.userDetailsService(userDetailsService);
    }

    //配置认证事件处理
    @Bean
    public AuthenticationEventPublisher authenticationEventPublisher(ApplicationEventPublisher applicationEventPublisher){

        return new DefaultAuthenticationEventPublisher(applicationEventPublisher);
    }

    //配置认证成功适配器
    @Bean
    public AuthenticationSuccessHandler successHandler(){
        AuthenticationSuccessHandler successHandler = new AuthenticationSuccessHandler(){

            @Override
            public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
                httpServletResponse.setCharacterEncoding("utf-8");
                httpServletResponse.setHeader("Content-Type","text/html;charset=UTF-8");
                String username = httpServletRequest.getParameter("username");
                SviUser user = sviUserService.getUserByUserName(username);
                Cookie cookie1 = new Cookie("userID",user.getId());
                Cookie cookie2 = new Cookie("name",username);
                cookie1.setHttpOnly(true);
                cookie1.setPath("/");
                cookie2.setPath("/");
                httpServletResponse.addCookie(cookie1);
                httpServletResponse.addCookie(cookie2);
                Map<String,Object> map = new HashMap<>();
                map.put("code",200);
                map.put("url","/");
                map.put("msg","认证成功");
                httpServletResponse.getWriter().write(JSONObject.toJSONString(map));
            }
        };
        return successHandler;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/test/**");
    }

}
