package com.shen.until.controller;

import com.shen.until.bean.User;
import com.shen.until.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("cache")
public class CacheTestController {

    @Autowired
    private CacheService cacheService;

    @GetMapping("test-Cache")
    public String getCache() {

        List<User> users = new ArrayList<>();
        if (users == null || users.isEmpty()){
            users = cacheService.userList();
        }
        return "success";
    }
}
