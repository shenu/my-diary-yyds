package com.shen.until.controller;

import com.shen.until.bean.xslx.OrderExcel;
import com.shen.until.common.Constant;
import com.shen.until.service.ExcelService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;

@RestController
@RequestMapping("excel")
public class OrderExcelController {

    @Autowired
    private ExcelService excelService;

    /**
     * 单线程  分批次提交 10000 13s
     *
     * 多线程 插入    5个线程 18s     32g 内存12线程 1.7s
     * 单线程  单条记录提交  40s
     * @author < shen
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("saveOneData")
    @SneakyThrows
    public String saveOneData(){

        long startTime = System.currentTimeMillis();
        List<OrderExcel> list = new ArrayList<>(Constant.max_size);
//        excelService.saveBatch(list);
//        list.clear();
        int num = 1;
        CountDownLatch countDownLatch = new CountDownLatch(5);
        for (int i = 0; i < 50000; i++) {
            OrderExcel orderExcel = new OrderExcel();
            orderExcel.setUserName("test");
            orderExcel.setOrderId(UUID.randomUUID().toString());
            list.add(orderExcel);
            if (list.size() >= Constant.max_size){

                List<OrderExcel> copyList = new CopyOnWriteArrayList<>(list);
                new Thread(()->{
                    excelService.saveBatch(copyList);
                    System.out.println(Thread.currentThread().getName());
                    countDownLatch.countDown();
                },"第"+String.valueOf(num)+"条线程：").start();
                num++;
                list.clear();
            }
        }

        countDownLatch.await();
        long endTime = System.currentTimeMillis();
        System.out.print("消耗时长为：");
        System.out.println(endTime-startTime);

        return "success";
    }
}
