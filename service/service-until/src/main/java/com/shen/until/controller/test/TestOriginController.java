package com.shen.until.controller.test;

import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther
 * @createdate 2023/5/31
 * @description
 */
@Controller
@RequestMapping("/test/origin/req")
public class TestOriginController {

    @SneakyThrows
    @RequestMapping("/goTrunReq")
    @ResponseBody
    @CrossOrigin
    public String goTrunReq(HttpServletRequest request, HttpServletResponse response){

//        response.setHeader("Access-Control-Allow-Credentials","true");
//        response.setHeader("Access-Control-Allow-Headers","accept,content-type");
//        response.setHeader("Access-Control-Allow-Methods","OPTIONS,GET,POST,DELETE,PUT");
//        response.setHeader("Access-Control-Allow-Origin","*");
        response.sendRedirect("http://localhost:8888/admin/welcome?service=https://baidu.com");
        Map<String, Object> result = new HashMap<>();
        result.put("code",200);
        result.put("msg","success");
        return JSONObject.toJSONString(result);
    }
}
