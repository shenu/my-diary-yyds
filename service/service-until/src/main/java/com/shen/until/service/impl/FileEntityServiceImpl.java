package com.shen.until.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shen.until.bean.FileEntity;
import com.shen.until.mapper.FileMapper;
import com.shen.until.service.FileEntityService;
import org.springframework.stereotype.Service;

@Service
public class FileEntityServiceImpl extends ServiceImpl<FileMapper, FileEntity> implements FileEntityService {
}
