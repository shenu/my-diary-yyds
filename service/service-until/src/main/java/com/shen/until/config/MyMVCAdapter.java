package com.shen.until.config;

import com.shen.until.interceptor.MyMVCIntercepter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMVCAdapter implements WebMvcConfigurer {

    @Autowired
    private MyMVCIntercepter myMVCIntercepter;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(myMVCIntercepter).addPathPatterns("/**");


    }
}
