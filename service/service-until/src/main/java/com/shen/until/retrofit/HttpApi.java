//package com.shen.until.retrofit;
//
//import com.github.lianjiatech.retrofit.spring.boot.core.RetrofitClient;
//import retrofit2.http.GET;
//import retrofit2.http.Query;
//
///**
// * @Auther
// * @createdate 2023/8/14
// * @description
// */
//@RetrofitClient(baseUrl = "http://localhost:8001/")
//public interface HttpApi {
//
//    @GET
//    public String getUserAll(@Query("current") Integer current,@Query("limit") Integer limit);
//}
