package com.shen.until.handler;

import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class MyAuthHandler {

    @EventListener
    public void onSuccess(AuthenticationSuccessEvent success) {
        // ...
        System.out.println("Auth success");
    }

    @EventListener
    public void onFailure(AbstractAuthenticationFailureEvent failures){
        // ...
//        throw
        System.out.println("Auth fail");
        throw new AuthenticationServiceException("passwd Error");

    }
}
