package com.shen.until.unitl;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class FileUntil {

    //将multipartFile 对象转化为file对象
    public static File multipartFileToFile(MultipartFile multipartFile){

        //获取文件名
        String filename = multipartFile.getOriginalFilename();
        //获取文件后缀
        String prefix = filename.substring(filename.lastIndexOf("."));
        try {
            File tempFile = File.createTempFile(filename, prefix);
            multipartFile.transferTo(tempFile);
            return tempFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

}
