package com.shen.until.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shen.until.bean.Category;
import com.shen.until.bean.Label;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
public interface LabelService extends IService<Label> {

}
