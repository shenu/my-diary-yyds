package com.shen.until.srpingsecurity;

import com.shen.until.bean.SviUser;
import com.shen.until.service.SviUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailImpl implements UserDetailsService {

    //注入用户service
    @Autowired
    private SviUserService sviUserService;

    @Override
    public User loadUserByUsername(String name) throws UsernameNotFoundException {
        SviUser user = sviUserService.getUserByUserName(name);
        if (user != null){
            UserBuilder builder = User.withDefaultPasswordEncoder();
            UserDetails build = builder.username(user.getUserName()).password(user.getPassword()).roles("USER", "ADMIN").build();
            return (User) build;
        }
        return null;
    }
}
