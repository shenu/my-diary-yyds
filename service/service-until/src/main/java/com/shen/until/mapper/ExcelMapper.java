package com.shen.until.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shen.until.bean.xslx.OrderExcel;

public interface ExcelMapper extends BaseMapper<OrderExcel> {
}
