package com.shen.until.controller;

import com.shen.until.bean.SviUser;
import com.shen.until.infocollect.anno.LoginAOPAnnotation;
import com.shen.until.service.SviUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private SviUserService userService;

    @GetMapping("login")
    public String login() {
        return "login";
    }

    @RequestMapping("/")
    @LoginAOPAnnotation(desc = "测试AOP注解:aaa")
    public String indexPage(){
        return "layout";
    }

    //错误页面
    @GetMapping("/error")
    public String getErrorPage() {
        return "error";
    }
    //布局
    @GetMapping("index")
    public String pageLayout(){

        return "index";
    }
    @GetMapping("login/loginToIndex")
    public String loginToIndex(){
        return "index";
    }

    //ajax请求登录
    @GetMapping("success-login")
    @ResponseBody
    public Map<String,Object> successLoginDefaultURL(HttpServletRequest request,HttpServletResponse response){
        Map<String, Object> map = new HashMap<>();
        String username = request.getParameter("username");
        SviUser user = userService.getUserByUserName(username);
        if (user != null){
            map.put("msg","登录成功");
            map.put("url","/");
            map.put("code",200);
            map.put("username",username);
        }
        Cookie cookie = new Cookie("userID",user.getId());
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        response.addCookie(cookie);
        return map;
    }
}
