package com.shen.until.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shen.until.bean.Category;
import com.shen.until.bean.Label;
import com.shen.until.mapper.CategoryMapper;
import com.shen.until.mapper.LabelMapper;
import com.shen.until.service.CategoryService;
import com.shen.until.service.LabelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
@Service
public class LabelServiceImpl extends ServiceImpl<LabelMapper, Label> implements LabelService {
}
