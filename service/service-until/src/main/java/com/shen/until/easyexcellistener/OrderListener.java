package com.shen.until.easyexcellistener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import com.shen.until.bean.xslx.OrderExcel;
import com.shen.until.service.ExcelService;
import com.shen.until.service.impl.ExcelServiceImpl;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
public class OrderListener implements ReadListener<OrderExcel> {


    private ExcelService excelService = new ExcelServiceImpl();

    //每隔5条存储数据库，实际使用中可以100条，然后清理list，方便内存回收
    private static final int BATCH_COUNT = 100;

    //缓存的数据
    private List<OrderExcel> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    @Override
    public void invoke(OrderExcel orderExcel, AnalysisContext analysisContext) {

        log.info("解析到一条数据：{}",orderExcel);
        cachedDataList.add(orderExcel);
        if (cachedDataList.size() >= BATCH_COUNT){
            log.info("存入数据库");
            //存储完成清理数据  list
            excelService.saveBatch(cachedDataList);
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * 所有数据解析完了 才会调用
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

        log.info("保存最后的数据");
        excelService.saveBatch(cachedDataList);
    }

    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        log.info("解析到一条头数据:{}", JSON.toJSONString(headMap));
        // 如果想转成成 Map<Integer,String>
        // 方案1： 不要implements ReadListener 而是 extends AnalysisEventListener
        // 方案2： 调用 ConverterUtils.convertToStringMap(headMap, context) 自动会转换
    }

    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {

        log.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
        // 如果是某一个单元格的转换异常 能获取到具体行号
        // 如果要获取头的信息 配合invokeHeadMap使用
        if (exception instanceof ExcelDataConvertException){
            ExcelDataConvertException excelDataConvertException =
                    (ExcelDataConvertException) exception;
            log.error(excelDataConvertException.getRowIndex().toString(),
                    excelDataConvertException.getColumnIndex(),
                    excelDataConvertException.getCellData());
        }
    }
}
