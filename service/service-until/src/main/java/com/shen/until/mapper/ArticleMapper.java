package com.shen.until.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shen.until.bean.Article;

public interface ArticleMapper extends BaseMapper<Article> {
}
