package com.shen.sviauth.controller;

import com.shen.common.utils.JwtUtils;
import com.shen.sviauth.client.UserClient;
import com.shen.sviauth.webpo.SviUser;
import org.apache.poi.hssf.record.UserSViewBegin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class LoginController {

    @Autowired
    private UserClient userClient;

//    private static String Statoken = null;

    //认证 获取token
    @PostMapping(value = "/admin/login",headers = {"Content-Type=application/x-www-form-urlencoded"})
    public String login(@RequestParam String username, @RequestParam(value = "password") String password
                        ,HttpServletResponse response){

        if (username.isEmpty() || password.isEmpty()){
            response.setStatus(400);
            return "用户名密码为空！";

        }else{
            SviUser oneUser = userClient.getOneUser(username, password);
            if(oneUser != null){
                
                if (!oneUser.getIsDisable().equals("true")){
                    String jwtToken = JwtUtils.getJwtToken(oneUser.getId(), oneUser.getUserName());

                    return jwtToken;
                }else {
                    response.setStatus(403);
                    return "用户以冻结";
                }
                
               
            }
        }
        response.setStatus(403);
        return "请求已拒绝";
    }


    //校验 token
    @GetMapping("admin/checkToken")
    public boolean checkToken(@RequestParam String token){


        if (!token.isEmpty())
            return JwtUtils.checkToken(token);
        return false;
    }

}
