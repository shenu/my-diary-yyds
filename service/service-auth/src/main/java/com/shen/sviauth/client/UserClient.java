package com.shen.sviauth.client;

import com.shen.sviauth.webpo.SviUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "service-user",fallback = UserClientImpl.class)
public interface UserClient {

    @PostMapping("api/entry/sviuser/getOneUser")
    public SviUser getOneUser(@RequestParam("name") String name,@RequestParam("pass") String pass);
}
