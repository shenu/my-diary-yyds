package com.shen.sviauth.config;

import com.shen.sviauth.handler.LoginHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class LoginConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private LoginHandler loginHandler;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        //配置请求拦截器并添加过滤项、排除项
//        registry.addInterceptor(loginHandler).addPathPatterns("/api/entry/**");
    }
}

