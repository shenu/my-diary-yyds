package com.shen.sviauth.handler;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;

@Configuration
public class LoginHandler implements HandlerInterceptor {



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        try {
            String reqToken = request.getHeader("token");
            HttpSession session = request.getSession();
            String setoken = session.getAttribute("token").toString();
            System.out.println("sessionToken->"+setoken);
            if (!reqToken.isEmpty() && reqToken.equals(setoken)){
                return true;
            }
            response.setStatus(401);
            response.setCharacterEncoding("utf-8");
            PrintWriter writer = response.getWriter();
            writer.println("请登录后读取数据");
            return false;
        } catch (Exception e) {
            response.setStatus(401);
            response.setCharacterEncoding("utf-8");
            PrintWriter writer = response.getWriter();
            writer.println("请登录后读取数据");
            return false;
        }

    }
}
