//package com.shen.test;
//
//import com.jcraft.jsch.Channel;
//import com.jcraft.jsch.ChannelSftp;
//import com.jcraft.jsch.JSch;
//import com.jcraft.jsch.Session;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//import java.util.Vector;
//
//public class FtpClient2 {
//
//    public static void main(String[] args) {
//        List<String> list = new ArrayList<String>();
//        ChannelSftp sftp = null;
//        Channel channel = null;
//        Session sshSession = null;
//        try {
//            JSch jsch = new JSch();
//            jsch.getSession("shen", "39.99.134.41", 22);
//            sshSession = jsch.getSession("shen", "39.99.134.41", 22);
//            sshSession.setPassword("123456");
//            Properties sshConfig = new Properties();
//            sshConfig.put("StrictHostKeyChecking", "no");
//            sshSession.setConfig(sshConfig);
//            sshSession.connect(10000);
//
//            channel = sshSession.openChannel("sftp");
//            channel.connect();
//            sftp = (ChannelSftp) channel;
//            Vector<?> vector = sftp.ls("/data/shen");
//            for (Object item:vector) {
//                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) item;
//                System.out.println(entry.getFilename());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            closeChannel(sftp);
//            closeChannel(channel);
//            closeSession(sshSession);
//        }
//    }
//
//    private static void closeChannel(Channel channel) {
//        if (channel != null) {
//            if (channel.isConnected()) {
//                channel.disconnect();
//            }
//        }
//    }
//
//    private static void closeSession(Session session) {
//        if (session != null) {
//            if (session.isConnected()) {
//                session.disconnect();
//            }
//        }
//    }
//}
