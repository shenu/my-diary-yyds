package com.shen.sviuser.service.impl;

import com.shen.sviuser.entity.SviPeople;
import com.shen.sviuser.mapper.SviPeopleMapper;
import com.shen.sviuser.service.SviPeopleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shen
 * @since 2022-01-11
 */
@Service
public class SviPeopleServiceImpl extends ServiceImpl<SviPeopleMapper, SviPeople> implements SviPeopleService {

}
