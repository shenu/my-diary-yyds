package com.shen.sviuser.config;

import com.shen.sviuser.handler.ReqHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class CheckReqConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private ReqHandler reqHandler;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(reqHandler).addPathPatterns("/api/entry/**")
        .excludePathPatterns("/api/entry/sviuser/getOneUser");
    }
}
