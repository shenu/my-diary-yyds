package com.shen.sviuser.service;

import com.shen.sviuser.entity.SviUser;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
public interface SviUserService extends IService<SviUser> {

//    查询所有用户带分页
    Map<String,Object> getUserListByPage(Integer current, Integer limit);

    //增加用户
    Integer addUser(SviUser user);
}
