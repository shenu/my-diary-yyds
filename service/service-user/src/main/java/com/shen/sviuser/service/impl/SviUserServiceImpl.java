package com.shen.sviuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shen.common.utils.MD5;
import com.shen.sviuser.entity.SviUser;
import com.shen.sviuser.entity.vo.UserVo;
import com.shen.sviuser.mapper.SviUserMapper;
import com.shen.sviuser.service.SviUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
@Service
public class SviUserServiceImpl extends ServiceImpl<SviUserMapper, SviUser> implements SviUserService {
    @Override
    public Map<String,Object> getUserListByPage(Integer current, Integer limit) {
        Page<SviUser> page = new Page<>(current,limit);
        QueryWrapper<SviUser> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("gmt_modified");
        IPage<SviUser> userIPage = baseMapper.selectPage(page, wrapper);
        long pages = userIPage.getPages();
        List<SviUser> users = userIPage.getRecords();
        List<UserVo> list = new ArrayList<>();
        users.forEach(user->{
            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(user,userVo);
            userVo.setDisable(Boolean.valueOf(user.getIsDisable()));
            list.add(userVo);
        });
        long total = userIPage.getTotal();
        Map<String,Object> map = new HashMap<>();
        map.put("pages",pages);
        map.put("users",list);
        map.put("total",total);
        return map;
    }

    //增加用户

    @Override
    public Integer addUser(SviUser user) {
        if (!user.getModifier().isEmpty() || !user.getCreator().isEmpty()){
            user.setPassword(MD5.encrypt(user.getPassword()));
            user.setNickName(user.getUserName());
            int insert = baseMapper.insert(user);
            return insert;
        }
        return 0;
    }
}
