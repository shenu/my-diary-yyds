package com.shen.sviuser.handler;

import com.shen.sviuser.client.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;

@Configuration
public class ReqHandler implements HandlerInterceptor {

    @Autowired
    private AuthClient authClient;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if ("OPTIONS".equals(request.getMethod())){
            return true;
        }
        String token = request.getHeader("Token");
        if (token.isEmpty()){
            response.setStatus(401);
            response.setCharacterEncoding("utf-8");
            PrintWriter writer = response.getWriter();
            writer.println("No identity information found");
            return false;
        }

        boolean b = authClient.checkToken(token);
        if (!b){
            response.setStatus(401);
            response.setCharacterEncoding("utf-8");
            PrintWriter writer = response.getWriter();
            writer.println("Identity verification failed");
            return false;
        }
        return true;
    }
}
