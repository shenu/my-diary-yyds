package com.shen.sviuser.controller;


import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author shen
 * @since 2022-01-11
 */
@RestController
@RequestMapping("/api/entry/svipeo/")

public class SviPeopleController {

    @NacosValue(value = "${nacosData}",autoRefreshed = true)
    private String nacosData;

    @RequestMapping("testNacos")
    public String testNacos(){
        return nacosData;
    }

}

