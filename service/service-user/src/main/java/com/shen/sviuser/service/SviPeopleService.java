package com.shen.sviuser.service;

import com.shen.sviuser.entity.SviPeople;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shen
 * @since 2022-01-11
 */
public interface SviPeopleService extends IService<SviPeople> {

}
