package com.shen.sviuser.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shen.sviuser.entity.SviUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
public interface SviUserMapper extends BaseMapper<SviUser> {

}
