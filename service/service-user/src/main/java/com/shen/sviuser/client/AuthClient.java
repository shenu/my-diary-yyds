package com.shen.sviuser.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Component
@FeignClient(value = "service-auth",fallback = AuthClientImpl.class)
public interface AuthClient {

    //token校验
    @GetMapping("/api/admin/checkToken")
    public boolean checkToken(@RequestParam String token);
}
