package com.shen.sviuser.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.shen.common.utils.MD5;
import com.shen.common.utils.R;
import com.shen.sviuser.entity.SviUser;
import com.shen.sviuser.service.SviUserService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author shen
 * @since 2022-01-05
 */
@RestController
@RequestMapping("/api/entry/sviuser/")
@CrossOrigin
public class SviUserController {

    @Autowired
    private SviUserService userService;


    @ApiModelProperty("查询所有用户带分页")
    @GetMapping("getUserListByPage/{current}/{limit}")
    public R getUserListByPage(@PathVariable Integer current,@PathVariable Integer limit){


        Map<String, Object> map = userService.getUserListByPage(current, limit);

        return R.ok().data(map);
    }

    @ApiModelProperty("查询单个用户，根据用户名密码,供给远程调用Auth")
    @PostMapping("getOneUser")
    public SviUser getOneUser( String name, String pass){

        QueryWrapper<SviUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name",name);
        wrapper.eq("password", MD5.encrypt(pass));
        SviUser user = userService.getOne(wrapper);
        return user;
    }

    @PostMapping("getUserInfo")
    @ApiOperation(value = "根据用户名获取用户信息")
    public SviUser getUserInfo(@RequestParam String name){

        QueryWrapper<SviUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name",name);
        SviUser user = userService.getOne(wrapper);
        //隐藏密码
        user.setPassword(null);
        return user;
    }

    //修改密码
    @ApiOperation("密码修改")
    @PutMapping("updatePass")
    @Transactional(rollbackFor=Exception.class)
    public R updateUserPass(@RequestParam String username
            ,@RequestParam String oldPass,@RequestParam String newPass){

        //校验
        QueryWrapper<SviUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name",username);
        wrapper.eq("password",MD5.encrypt(oldPass));
        SviUser user = userService.getOne(wrapper);
        if (user != null){
            boolean b = userService.updateById(user.setPassword(MD5.encrypt(newPass)));
            if (b){

                return R.ok().message("修改成功");
            }

        }

        return R.error().message("原密码错误");
    }


    //筛选 by Qualifications
    @ApiOperation("筛选 by Qualifications")
    @GetMapping("screenByQual")
    public R screenByQual(@RequestParam String q)  {

        if(q.contains("and")){
            try {
                Map<String,String> map = new HashMap<>();
                QueryWrapper<SviUser> wrapper = new QueryWrapper<>();
                String[] ands = q.split("and");
                for (String and : ands) {
                    String s = and.replaceAll("'", "").replaceAll("\"", "").trim();
                    String[] split = s.split("=");
                    map.put(split[0],split[1]);

                }
                wrapper.le("gmt_create",map.get("endDate"));
                wrapper.gt("gmt_create",map.get("startDate"));

                List<SviUser> users = userService.list(wrapper);
                return R.ok().data("users",users);
            } catch (Exception e) {
                e.printStackTrace();
                return R.error().message("参数错误");
            }


        }
        else {
            try {
                String[] split = q.replaceAll("'","").split("=");

                //获取拼接条件
                QueryWrapper<SviUser> wrapper = new QueryWrapper<>();
                wrapper.eq(split[0],split[1]);
                List<SviUser> users = userService.list(wrapper);


                return R.ok().data("users",users);
            } catch (Exception e) {
                e.printStackTrace();
                return R.error().message("参数错误");
            }
        }


    }


    ///新增用户
    @ApiOperation("新增用户")
    @PostMapping("user")
    @Transactional
    public R addUser(@RequestBody SviUser user){

        Integer flag = userService.addUser(user);
        if (flag == 1){
            return R.ok();
        }
        return R.error();
    }


    //图片上传到云服务器ECS
    @PostMapping("upload-avatar")
    @ApiOperation("头像上传")
    @Transactional
    public R uploadImageECS(@RequestBody MultipartFile file){

        String originalFilename = file.getOriginalFilename();
        System.out.println(originalFilename);
        return R.ok();
    }

    @ApiOperation("编辑用户信息")
    @PutMapping("user")
    @Transactional
    public R updateUser(){

        return R.ok();
    }

    @ApiOperation("删除用户信息")
    @DeleteMapping("user/{id}")
    @Transactional
    public R deleteUser(@PathVariable String id){
        userService.removeById(id);
        return R.ok();
    }
    @ApiOperation("更改用户状态")
    @PutMapping("user/{id}/{status}")
    @Transactional(rollbackFor = Exception.class)
    public R updateUserStatus(@PathVariable String id,@PathVariable  String status){

        SviUser user = userService.getById(id);
        user.setIsDisable(status);
        boolean b = userService.updateById(user);
        if (b){
            return R.ok();
        }
        return R.error();
    }
}

