package com.shen.sviuser.mapper;

import com.shen.sviuser.entity.SviPeople;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shen
 * @since 2022-01-11
 */
public interface SviPeopleMapper extends BaseMapper<SviPeople> {

}
